import {gql} from 'apollo-boost';

const getUsers = gql`
{
	users{
		id
		firstname
		lastname
		email
		username
		password
		address
		contact
		gender
		role
	}
}`
const getSingleUser = gql`
query($id:ID!){
	user(id: $id){
		id
		firstname
		lastname
		email
		username
		password
		address
		contact
		gender
		role
	}
}`

const getRoles = gql`
{
	roles{
		id
		role
	}
}`
const getSingleRole = gql`
query($id:ID!){
	role(id: $id){
		id
		role
	}
}`

const getRooms = gql`
{
	rooms{
		id
		room
		roomsize
		capacity
		imageLocation
	}
}`
const getSingleRoom = gql`
query($id:ID!){
	room(id: $id){
		id
		room
		roomsize
		capacity
		imageLocation
	}
}`

const getPaymentmodes = gql`
{
	paymentmodes{
		id
		mode
	}
}`
const getSinglePaymentmode = gql`
query($id:ID!){
	paymentmode(id: $id){
		id
		mode
	}
}`


const getBookings = gql`
{
	bookings{
		id
		userId
		name
		roomType
		bookDate
		bookTime
		tenure
		paymentmode
		paidamount
		paymentstatus
		bookstatus
		approvedby
	}
}`
const getSingleBooking = gql`
query($id:ID!){
	booking(id: $id){
		id
		userId
		name
		roomType
		bookDate
		bookTime
		tenure
		paymentmode
		paidamount
		paymentstatus
		bookstatus
		approvedby
	}
}`


export{
	getUsers,
	getSingleUser,

	getRoles,
	getSingleRole,

	getPaymentmodes,
	getSinglePaymentmode,

	getBookings,
	getSingleBooking,
	
	getRooms,
	getSingleRoom
}