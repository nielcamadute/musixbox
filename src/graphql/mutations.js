import {gql} from 'apollo-boost'

const userAddMutation = gql`
mutation
	(
		$firstname: String!, 
		$lastname: String!, 
		$email: String!, 
		$username: String!, 
		$password: String!, 
		$address: String!, 
		$contact: String!, 
		$gender: String!,
		$role: String!
	){

	addUser
		(
			firstname: $firstname, 
			lastname: $lastname, 
			email: $email, 
			username: $username, 
			password: $password, 
			address: $address, 
			contact: $contact,
			gender: $gender,
			role: $role
		){

			firstname
			lastname
			email
			username
			password
			address
			contact
			gender
			role
	}
}`
const userUpdateMutation = gql`
mutation
	(
		$id: ID!, 
		$firstname: String!, 
		$lastname: String!, 
		$email: String!, 
		$username: String!, 
		$password: String!, 
		$address: String!, 
		$contact: String!, 
		$gender: String!,
		$role: String!

	){
	updateUser
		(
			id: $id, 
			firstname: $firstname, 
			lastname: $lastname, 
			email: $email, 
			username: $username, 
			password: $password, 
			address: $address, 
			contact: $contact, 
			gender: $gender,
			role: $role
		){
			firstname
			lastname
			email
			username
			password
			address
			contact
			gender
			role
	}
}`
const userDeleteMutation = gql`
mutation($id: ID!){
	deleteUser(id: $id){
		id
	}
}`

const loginMutation = gql`
mutation ($email: String!, $password: String!) {
        login(email: $email, password: $password) {
        	id
            firstname
            role
            token
        }
    }
`

// ========================================================= Booking
const bookingAddMutation = gql`
mutation
	(
		$userId: String!, 
		$name: String!, 
		$roomType: String!, 
		$bookDate: String!, 
		$bookTime: String!, 
		$tenure: String, 
		$paymentmode: String, 
		$paidamount: String, 
		$paymentstatus: String, 
		$bookstatus: String, 
		$approvedby: String 
	){

	addBooking
		(
			userId: $userId,
			name: $name,
			roomType: $roomType,
			bookDate: $bookDate,
			bookTime: $bookTime,
			tenure: $tenure,
			paymentmode: $paymentmode,
			paidamount: $paidamount,
			paymentstatus: $paymentstatus,
			bookstatus: $bookstatus,
			approvedby: $approvedby
		){

			userId
			name
			roomType
			bookDate
			bookTime
			tenure
			paymentmode
			paidamount
			paymentstatus
			bookstatus
			approvedby
	}
}`
const bookingUpdateMutation = gql`
mutation
	(
		$id: ID!, 
		$userId: String!, 
		$name: String!, 
		$roomType: String!, 
		$bookDate: String!, 
		$bookTime: String!, 
		$tenure:String, 
		$paymentmode: String, 
		$paidamount:String, 
		$paymentstatus: String, 
		$bookstatus: String, 
		$approvedby: String 
	){
	updateBooking
		(
			id: $id, 
			userId: $userId,
			name: $name,
			roomType: $roomType,
			bookDate: $bookDate,
			bookTime: $bookTime,
			tenure: $tenure,
			paymentmode: $paymentmode,
			paidamount: $paidamount,
			paymentstatus: $paymentstatus,
			bookstatus: $bookstatus,
			approvedby: $approvedby
		){
			userId
			name
			roomType
			bookDate
			bookTime
			tenure
			paymentmode
			paidamount
			paymentstatus
			bookstatus
			approvedby
	}
}`
const bookingDeleteMutation = gql`
mutation($id: ID!){
	deleteBooking(id: $id){
		id
	}
}`


// ========================================================= ROOM
const roomAddMutation = gql`
mutation
	(
		$room: String!,
		$roomsize: String!,
		$capacity: Int!,
		$imageLocation: String
	){

	addRoom
		(
			room: $room,
			roomsize: $roomsize,
			capacity: $capacity,
			imageLocation: $imageLocation
		){

			room
			roomsize
			capacity
			imageLocation
	}
}`
const roomUpdateMutation = gql`
mutation
	(
		$id: ID!, 
		$room: String!,
		$roomsize: String!,
		$capacity: Int!,
		$imageLocation: String

	){
	updateRoom
		(
			id: $id, 
			room: $room,
			roomsize: $roomsize,
			capacity: $capacity,
			imageLocation: $imageLocation
		){
			room
			roomsize
			capacity
			imageLocation
	}
}`
const roomDeleteMutation = gql`
mutation($id: ID!){
	deleteRoom(id: $id){
		id
	}
}`

// ========================================================= ROLE
const roleAddMutation = gql`
mutation($role: String!){
	addRole(role: $role){role}
}`
const roleUpdateMutation = gql`
mutation($id: ID!, $role: String!){
	updateRole(id: $id, role: $role){role}
}`
const roleDeleteMutation = gql`
mutation($id: ID!){
	deleteRole(id: $id){id}
}`


// ========================================================= PAYMENTMODE
const paymentAddMutation = gql`
mutation($mode: String!){
	addPaymentmode(mode: $mode){mode}
}`
const paymentUpdateMutation = gql`
mutation($id: ID!, $mode: String!){
	updatePaymentmode(id: $id, mode: $mode){mode}
}`
const paymentDeleteMutation = gql`
mutation($id: ID!){
	deletePaymentmode(id: $id){id}
}`


export{
	userAddMutation,
	userUpdateMutation,
	userDeleteMutation,
	loginMutation,
	
	bookingAddMutation,
	bookingUpdateMutation,
	bookingDeleteMutation,

	roomAddMutation,
	roomUpdateMutation,
	roomDeleteMutation,

	roleAddMutation,
	roleUpdateMutation,
	roleDeleteMutation,

	paymentAddMutation,
	paymentUpdateMutation,
	paymentDeleteMutation,

}