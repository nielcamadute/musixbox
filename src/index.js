import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import ApolloClient from 'apollo-boost';
import {ApolloProvider} from 'react-apollo';
import Navbar from './components/Navbar'
import PageNotFound from './components/PageNotFound'
import RoomPage from './components/RoomPage'
import LoginPage from './components/LoginPage'
import AdminPage from './components/AdminPage'
import Register from './components/Register'
import CashierPage from './components/CashierPage'
import Catalog from './components/Catalog'
import CustomerPage from './components/CustomerPage'
import HomePage from './components/HomePage'

// const client = new ApolloClient({uri:'http://localhost:4000/graphql'})
const client = new ApolloClient({ uri: 'https://hidden-beach-00859.herokuapp.com/graphql'})
const pageComponent = (
	<React.Fragment>
		<ApolloProvider client={client}>
			<Router basename={process.env.PUBLIC_URL}>
				<Navbar/>
				<Switch>
					<Route exact path="/" component={HomePage}/>
					<Route exact path="/admin" component={AdminPage}/>
					<Route exact path="/cashier" component={CashierPage}/>
					<Route exact path="/customer" component={CustomerPage}/>
					<Route exact path="/register" component={ Register }/>
					<Route exact path="/login" component={ LoginPage }/>
				</Switch>
			</Router>
		</ApolloProvider>
	</React.Fragment>
)
ReactDOM.render(pageComponent, document.getElementById('root'));

