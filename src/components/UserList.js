import React, {useState} from 'react';
import {graphql} from 'react-apollo';
import Swal from 'sweetalert2'
import {flowRight as compose} from 'lodash'
import {getUsers} from '../graphql/queries'
import {userAddMutation, userUpdateMutation} from '../graphql/mutations'

const bcrypt = require('bcryptjs')
const UserList = (props)=>{
	const users = props.users
	const deleteUser = props.deleteUser
	const roles = props.roles

	// counter for error
	let countError = 0

	// for pop-up message for Successful or failure
	const Toast = Swal.mixin({
	 	toast: true,
		position: 'top-end',
		showConfirmButton: false,
		timer: 3000,
		timerProgressBar: true,
		onOpen: (toast) => {
			toast.addEventListener('mouseenter', Swal.stopTimer)
			toast.addEventListener('mouseleave', Swal.resumeTimer)
		}
	})


	// for select role option's data
	let roleOptions = <option value="">Loadings</option>
	if (roles !== undefined) {
		roleOptions = roles.map(role => <option key={role.id} value={role.role}>{role.role}</option>)
	}

	const [firstname, setfirstname] = useState('');
	const [lastname, setlastname] = useState('');
	const [email, setemail] = useState('');
	const [username, setusername] = useState('');
	const [password, setpassword] = useState('');
	const [confirm, setconfirm] = useState('');
	const [address, setaddress] = useState('');
	const [contact, setcontact] = useState('');
	const [gender, setgender] = useState('');
	const [role, setrole] = useState('');

	const newUser = {
		firstname: firstname,
		lastname: lastname,
		email: email,
		username: username,
		password: password,
		address: address,
		contact: contact,
		gender: gender,
		role: role
	}
	const addUser = (e, newUser)=>{
		e.preventDefault()
		let checkemail = email.toLowerCase();
		let userlist;

		// to check if empty or not
		if(firstname === ""){
			Toast.fire({ title: 'Firstname field is empty', icon: 'error'})
			countError += 1
		}
		if(lastname === ""){
			Toast.fire({ title: 'Lastname field is empty', icon: 'error'})
			countError += 1
		}
		if(email === ""){
			Toast.fire({ title: 'Email field is empty', icon: 'error'})
			countError += 1
		}
		if(username === ""){
			Toast.fire({ title: 'Username field is empty', icon: 'error'})
			countError += 1
		}
		if(password === ""){
			Toast.fire({ title: 'Password field is empty', icon: 'error'})
			countError += 1
		}
		if(address === ""){
			Toast.fire({ title: ' field is empty', icon: 'error'})
			countError += 1
		}
		if(contact === ""){
			Toast.fire({ title: 'Contact field is empty', icon: 'error'})
			countError += 1
		}
		if(gender === ""){
			Toast.fire({ title: 'Gender field is empty', icon: 'error'})
			countError += 1
		}
		if(role === ""){
			Toast.fire({ title: 'Role field is empty', icon: 'error'})
			countError += 1
		}

		if (users !== undefined) {
			userlist = users.map(u =>{
				const{email} = u

				// convert data to lowercase for checking
				let lcaseemail = email.toLowerCase()

				if(lcaseemail === checkemail){
					Toast.fire({ title: 'Email Address is already used', icon: 'error'})
					countError += 1
				}
			})
		}

		// check if countError is zero, then add
		if(countError === 0){
			props.userAddMutation({
				variables: newUser,
				refetchQueries: [{query:getUsers}]
			}).then((response)=>{
				const roleAdded = response.data.addUser

				if (roleAdded) {
					Toast.fire({
		                title: 'Successfully Added',
		                icon: 'success'
		            }).then(() => {
		            	setfirstname('')
		            	setlastname('')
		            	setemail('')
		            	setusername('')
		            	setpassword('')
		            	setconfirm('')
		            	setaddress('')
		            	setcontact('')
		            	setgender('')
		            	setrole('')
		            	// window.location.href = './admin'
		            	window.location.reload()
		            })
				}
				else{
					Toast.fire({
	                    title: 'Failed to add data',
	                    icon: 'error'
	                })
				}
			})
		}
	}

	// delete/remove the copied user data in localstorage
	let deleteUserCopy = ()=>{
		localStorage.removeItem('getuserid')
		localStorage.removeItem('getfirstnameuser')
		localStorage.removeItem('getlastname')
		localStorage.removeItem('getemail')
		localStorage.removeItem('getusername')
		localStorage.removeItem('getpassword')
		localStorage.removeItem('getconfirm')
		localStorage.removeItem('getcontact')
		localStorage.removeItem('getaddress')
		localStorage.removeItem('getgender')
		localStorage.removeItem('getrole')
	}

	// temporarily retrieve and set data for update
	const[updateid, setupdateid] = useState('')
	const[updatefirstname, setupdatefirstname] = useState('')
	const[updatelastname, setupdatelastname] = useState('')
	const[updateemail, setupdateemail] = useState('')
	const[updateusername, setupdateusername] = useState('')
	const[updatepassword, setupdatepassword] = useState('')
	const[updateconfirm, setupdateconfirm] = useState('')
	const[updatecontact, setupdatecontact] = useState('')
	const[updateaddress, setupdateaddress] = useState('')
	const[updategender, setupdategender] = useState('')
	const[updaterole, setupdaterole] = useState('')

	
	/*to check if the password is old or new; if new, it will be encrypted*/
	let finalUpdatedPassword;
	if(updatepassword === localStorage.getItem('getpassword')){
		finalUpdatedPassword = localStorage.getItem('getpassword')
	}
	else{
		finalUpdatedPassword = bcrypt.hashSync(updatepassword)
	}

	// update function
	const userUpd = {
		id: updateid,
		firstname: updatefirstname,
		lastname: updatelastname,
		email: updateemail,
		username: updateusername,
		password: finalUpdatedPassword,
		address: updateaddress,
		contact: updatecontact,
		gender: updategender,
		role: updaterole
	}
	const updateUser = (e, userUpd)=>{
		e.preventDefault()
		props.userUpdateMutation({
			variables: userUpd
		}).then(()=>{
			Toast.fire({
				icon: 'success',
				title: 'Successfully updated!'
			})
			deleteUserCopy()
			// window.location.href = './admin'
			window.location.reload()

		})
	}



	// Employee List
	let employeelist = <li className="ring ring-margin">Loading</li>
	if (users !== undefined) {
		employeelist = users.map((user)=>{
			const{id,firstname, lastname, role}  = user

			if(role !== "Customer"){
				// temporarily save/copy user data to localstorage for update purposes
				let userCopy = ()=>{
					localStorage.setItem('getuserid', user.id)
					localStorage.setItem('getfirstnameuser', user.firstname)
					localStorage.setItem('getlastname', user.lastname)
					localStorage.setItem('getemail', user.email)
					localStorage.setItem('getusername', user.username)
					localStorage.setItem('getpassword', user.password)
					localStorage.setItem('getconfirm', user.password)
					localStorage.setItem('getcontact', user.contact)
					localStorage.setItem('getaddress', user.address)
					localStorage.setItem('getgender', user.gender)
					localStorage.setItem('getrole', user.role)
					setupdateid(localStorage.getItem('getuserid'))
					setupdatefirstname(localStorage.getItem('getfirstnameuser'))
					setupdatelastname(localStorage.getItem('getlastname'))
					setupdateemail(localStorage.getItem('getemail'))
					setupdateusername(localStorage.getItem('getusername'))
					setupdatepassword(localStorage.getItem('getpassword'))
					setupdateconfirm(localStorage.getItem('getconfirm'))
					setupdatecontact(localStorage.getItem('getcontact'))
					setupdateaddress(localStorage.getItem('getaddress'))
					setupdategender(localStorage.getItem('getgender'))
					setupdaterole(localStorage.getItem('getrole'))
				}
				return(
					<li key={id}>
			            <span>{firstname} {lastname}</span>
			            <span className="mobile-hide">{role}</span>
			            <span className="btn-group mobile-adjust" role="group">
			            	<button className="btn btn-item" data-toggle="modal" data-target="#modal-user-update" onClick={()=>userCopy()}>
			            		<i className="far fa-edit"></i>
			            	</button>
			            	<button className="btn btn-item" onClick={()=>deleteUser(id)}>
			            		<i className="far fa-trash-alt"></i>
			            	</button>
			            </span>
			        </li>
				)
			}
		})
	}


	// Customer List
	let customerlist = <li className="ring ring-margin">Loading</li>
	if (users !== undefined) {
		customerlist = users.map((user)=>{
			const{id,firstname, lastname, role}  = user
			if(role === "Customer"){
				return(
					<li key={id}>
			            <span>{firstname} {lastname}</span>
			            <span className="mobile-hide">{role}</span>
			            <span>
			            	<button className="btn btn-item mobile-adjust-single" onClick={()=>deleteUser(id)}>
			            		<i className="far fa-trash-alt"></i>
			            	</button>
			            </span>
			        </li>
				)
			}
		})
	}

	return(
		<div className="row row-user" id="user-row">
		    <div className="col-lg-12 col-md-12 col-xs-12" id="employeelist-col">  
				<ul className="user-ul">
				    <div className="user-div">
						<span><h3>Employee</h3></span>
						<span></span>
					</div>
					<li id="user-list-label">
			            <span>Name</span>
			            <span className="mobile-hide">Position</span>
			            <span>
			            	<a type="button" className="mobile-adjust" data-toggle="modal" data-target="#modal-user">
					            <i className="fas fa-plus" id="user-add-icon"></i>
							</a>
			            </span>
			        </li>      
					{employeelist}

					<React.Fragment>
						<div className="modal fade" tabIndex="-1" id="modal-user">
							<div className="modal-dialog modal-lg">
								<div className="modal-content">
									<div className="modal-header mode-modal-header" id="">
										<h6 className="modal-title">Add New User</h6>
										<button type="button" className="close" data-dismiss="modal">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div className="modal-body">
										<form action="" className="form" onSubmit={(e)=>addUser(e,newUser)} id="user-form">
											<div className="row">
											    <div className="col-lg-6 col-md-12 col-xs-12 col-form-user">
												    <div className="form-group">
												        <label htmlFor="">First Name</label>
												        <input type="text" className="form-control" id="firstname" name="firstname" value={firstname} onChange={(e)=> setfirstname(e.target.value)} required/>
												    </div>
												    <div className="form-group">
												        <label htmlFor="">Last Name</label>
												        <input type="text" className="form-control" id="lastname" name="lastname" value={lastname} onChange={(e)=> setlastname(e.target.value)} required/>
												    </div>
												    <div className="form-group">
												        <label htmlFor="">Email</label>
												        <input type="email" className="form-control" id="email" name="email" value={email} onChange={(e)=> setemail(e.target.value)} required/>
												    </div>
												    <div className="form-group">
												        <label htmlFor="">Username</label>
												        <input type="text" className="form-control" id="username" name="username" value={username} onChange={(e)=> setusername(e.target.value)} required/>
												    </div>
												    <div className="form-group">
												        <label htmlFor="">Password</label>
												        <input type="password" className="form-control" id="password" name="password" value={password} onChange={(e)=> setpassword(e.target.value)} required/>
												    </div>
											    </div>        
											    <div className="col-lg-6 col-md-12 col-xs-12 col-form-user">
												    <div className="form-group">
												        <label htmlFor="">Address</label>
												        <input type="text" className="form-control" name="address" id="address" value={address} onChange={(e)=> setaddress(e.target.value)} required/>
												    </div>
												    <div className="form-group">
												        <label htmlFor="">Contact</label>
												        <input type="text" className="form-control" id="contact" name="contact" value={contact} onChange={(e)=> setcontact(e.target.value)} required/>
												    </div>
												    <div className="form-group">
												        <label htmlFor="gender">Gender</label>
												        <select className="form-control" name="gender" id="gender" value={gender} onChange={(e)=> setgender(e.target.value)} required>
												            <option value="">Select</option>
												            <option value="Male">Male</option>
												            <option value="Female">Female</option>
												        </select>
												    </div>
												    <div className="form-group">
												        <label htmlFor="role">Role</label>
												        <select className="form-control" name="role" id="role" value={role} onChange={(e)=> setrole(e.target.value)} required>
												            <option value="select">Select</option>
												            {roleOptions}
												        </select>
												    </div>
												    <button type="submit" className="btn" id="user-modal-submit">Submit</button>
											    </div>        
											</div>
										</form>
									</div>	
								</div>
							</div>
						</div>

						<div className="modal fade" tabIndex="-1" id="modal-user-update">
							<div className="modal-dialog modal-lg">
								<div className="modal-content">
									<div className="modal-header mode-modal-header" id="">
										<h6 className="modal-title">Update User</h6>
										<button type="button" className="close" data-dismiss="modal" onClick={()=>deleteUserCopy()}>
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div className="modal-body">
										<form action="" className="form" onSubmit={(e)=>updateUser(e,userUpd)} id="user-form-update">
											<div className="row">
											    <div className="col-lg-6 col-md-12 col-xs-12 col-form-user">
												    <div className="form-group" hidden>
												        <label htmlFor="">User ID</label>
												        <input type="text" className="form-control" id="updateid" name="updateid" value={updateid} onChange={(e)=> setupdateid(e.target.value)} required/>
												    </div>
												    <div className="form-group">
												        <label htmlFor="">First Name</label>
												        <input type="text" className="form-control" id="updatefirstname" name="updatefirstname" value={updatefirstname} onChange={(e)=> setupdatefirstname(e.target.value)} required/>
												    </div>
												    <div className="form-group">
												        <label htmlFor="">Last Name</label>
												        <input type="text" className="form-control" id="updatelastname" name="updatelastname" value={updatelastname} onChange={(e)=> setupdatelastname(e.target.value)} required/>
												    </div>
												    <div className="form-group">
												        <label htmlFor="">Email</label>
												        <input type="email" className="form-control" id="updateemail" name="updateemail" value={updateemail} onChange={(e)=> setupdateemail(e.target.value)} required/>
												    </div>
												    <div className="form-group">
												        <label htmlFor="">Username</label>
												        <input type="text" className="form-control" id="updateusername" name="updateusername" value={updateusername} onChange={(e)=> setupdateusername(e.target.value)} required/>
												    </div>
												    <div className="form-group">
												        <label htmlFor="">Password</label>
												        <input type="password" className="form-control" id="updatepassword" name="updatepassword" value={updatepassword} onChange={(e)=> setupdatepassword(e.target.value)} required/>
												    </div>
											    </div>        
											    <div className="col-lg-6 col-md-12 col-xs-12 col-form-user">
												    <div className="form-group">
												        <label htmlFor="">Address</label>
												        <input type="text" className="form-control" name="updateaddress" id="updateaddress" value={updateaddress} onChange={(e)=> setupdateaddress(e.target.value)} required/>
												    </div>
												    <div className="form-group">
												        <label htmlFor="">Contact</label>
												        <input type="text" className="form-control" id="updatecontact" name="updatecontact" value={updatecontact} onChange={(e)=> setupdatecontact(e.target.value)} required/>
												    </div>
												    <div className="form-group">
												        <label htmlFor="gender">Gender</label>
												        <select className="form-control" name="updategender" id="updategender" value={updategender} onChange={(e)=> setupdategender(e.target.value)} required>
												            <option value="">Select</option>
												            <option value="Male">Male</option>
												            <option value="Female">Female</option>
												        </select>
												    </div>
												    <div className="form-group">
												        <label htmlFor="role">Role</label>
												        <select className="form-control" name="updaterole" id="updaterole" value={updaterole} onChange={(e)=> setupdaterole(e.target.value)} required>
												            <option value="select">Select</option>
												            {roleOptions}
												        </select>
												    </div>
												    <button type="submit" className="btn" id="user-modal-update">Update</button>
											    </div>        
											</div>
										</form>
									</div>	
								</div>
							</div>
						</div>
					</React.Fragment>
				</ul>
		    </div>
		    <div className="col-lg-12 col-md-12 col-xs-12" id="customerlist-col">  
				<ul className="user-ul">
				    <div className="user-div">
						<span><h3>Customer</h3></span>
						<span></span>
					</div>
					<li id="user-list-label">
			            <span>Name</span>
			            <span className="mobile-hide">Position</span>
			            <span>
			            </span>
			        </li>
			        {customerlist}      
				</ul>
		    </div>
		</div>
	)
}
export default compose(
	graphql(getUsers, {name: 'getUsers'}),
	graphql(userAddMutation, {name: 'userAddMutation'}),
	graphql(userUpdateMutation, {name: 'userUpdateMutation'}),
)(UserList)