import React from 'react';
import ReactDOM from 'react-dom';

const Footer = ()=>{
	return(
		<div className="container-fluid component-footer" id="footer-container">
			<div className="row">
			    <div className="col-lg-4 col-md-4 col-xs-12">
			    	<p>Links</p>
			    	<div><a href="./">Home</a></div>
			    	<div><a href="./#catalog-container">Catalog</a></div>
			    	<div><a href="./register">Register</a></div>
			    	<div><a href="./login">Login</a></div>
			    </div>
			    <div className="col-lg-4 col-md-4 col-xs-12">
			    	<p>About</p>
			    	<div>
			    		This is a booking site for Musixbox ktv bar where you can reserve a room for your party or event needs.
			    	</div>

			    </div>
			    <div className="col-lg-4 col-md-4 col-xs-12">
			    	<p>Contact</p>
			    	<div><i className="fas fa-map-marker-alt"></i>Washington St., Pio del Pilar, Makati City, PH</div>
			    	<div><i className="fas fa-at"></i>nielcam88@gmail.com</div>
			    	<div><i className="fas fa-mobile-alt"></i>09154071784</div>
			    	<div></div>
			    </div>
			    <div className="col-lg-12 col-md-12 col-xs-12" id="copyright-col">
			    	<p>
			    		<i className="far fa-copyright"></i>2020 musixbox
			    	</p>
			    </div>
			    
		  	</div>
		</div>
	)
}
export default Footer





