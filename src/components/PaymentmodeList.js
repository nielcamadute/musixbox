import React, {useState} from 'react';
import {graphql} from 'react-apollo';
// import {Link} from 'react-router-dom'
import Swal from 'sweetalert2'
import {flowRight as compose} from 'lodash' 
import {getPaymentmodes, getRoles} from '../graphql/queries'
import {
	paymentAddMutation,
	paymentUpdateMutation, 
	roleAddMutation,
	roleUpdateMutation
} from '../graphql/mutations'

const PaymentmodeList = (props)=>{
	const paymentmodes = props.paymentmodes
	const deletePaymentmode = props.deletePaymentmode
	const roles = props.roles
	const deleteRole = props.deleteRole

	// counter for error
	let countError = 0

	// for pop-up message for Successful or failure
	const Toast = Swal.mixin({
	 	toast: true,
		position: 'top-end',
		showConfirmButton: false,
		timer: 3000,
		timerProgressBar: true,
		onOpen: (toast) => {
			toast.addEventListener('mouseenter', Swal.stopTimer)
			toast.addEventListener('mouseleave', Swal.resumeTimer)
		}
	})

	// ============================================================================================ PAYMENT MODE

	// add function for payment mode
	const [mode, setmode] = useState('')
	const newMode = {mode: mode}
	let addPaymentmode = (e, newMode)=>{
		e.preventDefault()
		let modelist;

		// convert data to lowercase for checking
		let checkmode = mode.toLowerCase();

		// to check if empty or not
		if(mode === ""){
			Toast.fire({ title: 'Please input new payment mode', icon: 'error'})
			countError += 1
		}


		// to check if the payment mode is available or not
		if (paymentmodes !== undefined) {
			modelist = paymentmodes.map(m =>{
				const{mode} = m

				// convert data to lowercase for checking
				let lcasemode = mode.toLowerCase()

				if(lcasemode === checkmode){
					Toast.fire({ title: 'Mode already exist', icon: 'error'})
					countError += 1
				}
			})
		}

		// if there is no error, data will be added
		if(countError === 0){
			props.paymentAddMutation({
				variables: newMode,
				refetchQueries: [{query:getPaymentmodes}]
			}).then((response)=>{
				const modeAdded = response.data.addPaymentmode
				if (modeAdded) {
					Toast.fire({
		                title: 'Successfully added!',
		                icon: 'success'
		            }).then(() => {
		            	setmode('')
		            	// window.location.href = './admin'
		            	window.location.reload()
		            })
				}
				else{
					Toast.fire({
	                    title: 'Failed to add data',
	                    icon: 'error'
	                })
				}
			})
		}
	}




	// this function use to delete/remove copied payment mode data in localstorage
	let deleteModeCopy = ()=>{
		localStorage.removeItem("getmodeid");
		localStorage.removeItem('getmode');
	}

	// usestate use to retrieve temporary payment mode data to update
	const [modeIdValue, setmodeIdValue] = useState('')
	const [modeValue, setmodeValue] = useState('')
	
	// function to update payment mode data
	const modeUpd = {id: modeIdValue, mode: modeValue}
	const updatePaymentmode = (e, modeUpd)=>{
		e.preventDefault()
		props.paymentUpdateMutation({
			variables: modeUpd
		}).then(()=>{
			Toast.fire({
                title: 'Successfully updated!',
                icon: 'success'
            })
			deleteModeCopy()
			// window.location.href = './admin'
			window.location.reload()
		})
	}

	// payment mode list
	let listmode = <li className="ring">Loading</li>
	if (paymentmodes !== undefined) {
		listmode = paymentmodes.map((modes)=>{
			// temporarily copy the data to localstorage for update purposes
			let modeCopy = ()=>{
				localStorage.setItem("getmodeid", modes.id);
				localStorage.setItem("getmode", modes.mode);
				setmodeIdValue(localStorage.getItem("getmodeid"))
				setmodeValue(localStorage.getItem("getmode"))
			}
			const{id, mode} = modes
			return(
				<li key={id}>
					<span className="mode">{mode}</span>
					<span className="btn-group" role="group" aria-label="Basic example">
		            	<button className="btn btn-item" data-toggle="modal" data-target="#modal-mode-update" onClick={()=>modeCopy()}>
		            		<i className="far fa-edit"></i>
		            	</button>
		            	<button className="btn btn-item" onClick={()=>deletePaymentmode(id)}>
			            	<i className="far fa-trash-alt"></i>
		            	</button>
					</span>
				</li>
			)
		})
	}

	


	// ============================================================================================ ROLE

	// add function for role
	const [role, setrole] = useState('')
	const newRole = {role: role}
	const addRole = (e, newRole)=>{
		e.preventDefault()
		let rolelist;

		// convert data to lowercase for checking
		let checkrole = role.toLowerCase();

		// to check if empty or not
		if(role === ""){
			Toast.fire({ title: 'Please input new role', icon: 'error'})
			countError += 1
		}


		// to check if the role is available or not
		if(roles !== undefined) {
			rolelist = roles.map(r =>{
				const{role} = r

				// convert data to lowercase for checking
				let lcaserole = role.toLowerCase()
				
				if(lcaserole === checkrole){
					Toast.fire({ title: 'Role already exist', icon: 'error'})
					countError += 1
					console.log(countError + " error")
				}
			})
		}

		// if countError value is zero, data will be added
		if(countError === 0){
			props.roleAddMutation({
				variables: newRole,
				refetchQueries: [{query:getRoles}]
			}).then((response)=>{
				const roleAdded = response.data.addRole
				if (roleAdded) {
					Toast.fire({
		                title: 'Successfully added!',
		                icon: 'success'
		            }).then(() => {
		            	setrole('')
		            })
				}
				else{
					Toast.fire({
	                    title: 'Failed to add data',
	                    icon: 'error'
	                })
				}
			})
		}
	}

	// this function use to delete/remove copied role data in localstorage
	let deleteRoleCopy = ()=>{
		localStorage.removeItem("getroleid");
		localStorage.removeItem('getrole');
	}

	// usestate use to retrieve temporary role data to update
	const [roleIdValue, setroleIdValue] = useState('')
	const [roleValue, setroleValue] = useState('')

	// function to update role data
	const roleUpd = {id: roleIdValue, role: roleValue}
	const updateRole = (e, roleUpd)=>{
		e.preventDefault()
		props.roleUpdateMutation({
			variables: roleUpd
		}).then(()=>{
			Toast.fire({
                title: 'Successfully updated!',
                icon: 'success'
            })
			deleteRoleCopy()
			// window.location.href = './admin'
			window.location.reload()
		})
	}



	// role list
	let listroles = <li className="ring">Loading</li>
	if (roles !== undefined) {
		listroles = roles.map((rol)=>{
			const{id, role} = rol

			// function temporarily save/copy the role data
			let roleCopy = ()=>{
				localStorage.setItem("getroleid", rol.id);
				localStorage.setItem("getrole", rol.role);
				setroleIdValue(localStorage.getItem("getroleid"))
				setroleValue(localStorage.getItem("getrole"))
			}

			return(
				<li key={id}>
		            <span>{role}</span>
		            <span className="btn-group" role="group" aria-label="Basic example">
		            	<button className="btn btn-item" data-toggle="modal" data-target="#modal-role-update" onClick={()=>roleCopy()}>
		            		<i className="far fa-edit"></i>
		            	</button>
			        	<button className="btn btn-item" onClick={()=>deleteRole(id)}>
			        		<i className="far fa-trash-alt"></i>
			        	</button>
		            </span>
		        </li>
			)
		})
	}

	return(
		<div className="row maintenance" id="">
		    <div className="col-lg-6 col-md-12 col-xs-12" id="payment-col">        
				<ul className="paymentmode-ul">
					<div className="paymentmode-div">
 						<span><h4>Payment Mode</h4></span>
 						<span>
							<a type="button" className="" data-toggle="modal" data-target="#modal-mode">
								<i className="fas fa-plus"></i>
							</a>
 						</span>
 					</div>
					<React.Fragment>
						
						<div className="modal fade" tabIndex="-1" id="modal-mode">
							<div className="modal-dialog">
								<div className="modal-content">
									<div className="modal-header mode-modal-header" id="mode-modal-header">
										<h6 className="modal-title">Add New Payment Mode</h6>
										<button type="button" className="close" data-dismiss="modal">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div className="modal-body">
										<form action="" className="form" onSubmit={(e)=>addPaymentmode(e,newMode)}>
											<div className="row">
												<div className="col-lg-12 col-md-12 col-xs-12">
												    <div className="form-group">
												        <input type="text" className="form-control" id="mode" name="mode" value={mode} onChange={(e)=> setmode(e.target.value)} placeholder="Payment Mode" required/>
												    </div>			    
												    <button type="submit" className="btn" id="mode-modal-submit">Submit</button>
												</div>
											</div>
										</form>
									</div>	
								</div>
							</div>
						</div>

						<div className="modal fade" tabIndex="-1" id="modal-mode-update">
							<div className="modal-dialog">
								<div className="modal-content">
									<div className="modal-header mode-modal-header">
										<h6 className="modal-title">Update Payment Mode</h6>
										<button type="button" className="close" data-dismiss="modal" onClick={()=>deleteModeCopy()}>
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div className="modal-body">
										<form action="" className="form" onSubmit={(e)=>updatePaymentmode(e,modeUpd)}>
											<div className="row">
												<div className="col-lg-12 col-md-12 col-xs-12">
												    <div className="form-group" hidden>
												        <input type="text" className="form-control" id="updateModeId" name="updateModeId" value={modeIdValue} onChange={(e)=> setmodeIdValue(e.target.value)} placeholder="Payment Mode Id" required/>
												    </div>
												    <div className="form-group">
												        <input type="text" className="form-control" id="updateMode" name="updateMode" value={modeValue} onChange={(e)=> setmodeValue(e.target.value)} placeholder="Payment Mode" required/>
												    </div>			    
												    <button type="submit" className="btn" id="mode-modal-update">Update</button>
												</div>
											</div>
										</form>
									</div>	
								</div>
							</div>
						</div>
					</React.Fragment>
					{listmode}
				</ul>
		    </div>
		    <div className="col-lg-6 col-md-12 col-xs-12">
			    <ul className="role-ul">
					<div className="role-div">
 						<span><h4>Role</h4></span>
 						<span>
							<a type="button" className="" data-toggle="modal" data-target="#modal-role">
								<i className="fas fa-plus"></i>
							</a>
 						</span>
 					</div>
					<React.Fragment>
						<div className="modal fade" tabIndex="-1" id="modal-role">
							<div className="modal-dialog">
								<div className="modal-content">
									<div className="modal-header role-modal-header" id="">
										<h6 className="modal-title">Add New Role</h6>
										<button type="button" className="close" data-dismiss="modal">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div className="modal-body">
										<form action="" className="form role-form" onSubmit={(e)=>addRole(e,newRole)} id="role-add">
											<div className="row">
											    <div className="col-lg-12 col-md-12 col-xs-12"> 
												    <div className="form-group">
												        <input type="text" className="form-control" id="nrole" name="nrole" value={role} onChange={(e)=> setrole(e.target.value)} placeholder = "Role" required/>
												    </div>			    
												    <button type="submit" className="btn" id="role-modal-submit">Submit</button>
											    </div> 
										    </div> 
										</form>
									</div>	
								</div>
							</div>
						</div>	
			            <div className="modal fade" tabIndex="-1" id="modal-role-update">
							<div className="modal-dialog">
								<div className="modal-content">
									<div className="modal-header role-modal-header" id="">
										<h6 className="modal-title">Update Role</h6>
										<button type="button" className="close" data-dismiss="modal" onClick={()=>deleteRoleCopy()}>
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div className="modal-body">
										<form action="" className="form role-form" onSubmit={(e)=>updateRole(e,roleUpd)} id="role-update">
											<div className="row">
											    <div className="col-lg-12 col-md-12 col-xs-12"> 
												    <div className="form-group" hidden>
												        <input type="text" className="form-control" id="updateId" name="updateId" value={roleIdValue} onChange={(e)=> setroleIdValue(e.target.value)} placeholder = "Id" required/>
												    </div>
												    <div className="form-group">
												        <input type="text" className="form-control" id="updateRole" name="updateRole" value={roleValue} onChange={(e)=> setroleValue(e.target.value)} placeholder = "Role" required/>
												    </div>			    
												    <button type="submit" className="btn" id="role-modal-update">Update</button>
											    </div> 
										    </div> 
										</form>
									</div>	
								</div>
							</div>
						</div>
					</React.Fragment>
					{listroles}
				</ul>        
		    </div>
		</div>
	)
}
export default compose(
	graphql(getPaymentmodes, {name: 'getPaymentmodes'}),
	graphql(paymentAddMutation, {name: 'paymentAddMutation'}),
	graphql(paymentUpdateMutation, {name: 'paymentUpdateMutation'}),
	graphql(roleAddMutation, {name: 'roleAddMutation'}),
	graphql(roleUpdateMutation, {name: 'roleUpdateMutation'}),
	graphql(getRoles, {name: 'getRoles'})
)(PaymentmodeList)