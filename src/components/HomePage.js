import React from 'react';
import Catalog from './Catalog'
import Footer from './Footer';


const HomePage = ()=>{

	// <h1 id="under-h1">musixbox</h1>
	return(
		<React.Fragment>
			<div className="container-fluid" id="home-container">
				<div className="row">
					<div className="col-lg-12 col-md col-xs">
						<h1>musixb</h1>
						<h1 id="under-h1">x</h1>
						<p id="p-home1">
							a place where you can sing and groove!
						</p>
						<a id="p-home2" href="./#catalog-container">
							<div className="mouse-scroll hidden-xs">
	                            <span className="mouse">
	                                <span className="mouse-movement"></span>
	                            </span>
	                        </div>
						</a>
					</div>
				</div>
			</div>
			<Catalog/>
			<Footer/>
		</React.Fragment>
	)
}
export default HomePage
