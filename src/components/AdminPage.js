import React from 'react';
import Swal from 'sweetalert2'
import PaymentmodeList from './PaymentmodeList';
// import RoleList from './RoleList'
import RoomList from './RoomList'
import Footer from './Footer';
import UserList from './UserList'
import {graphql} from 'react-apollo';
// import axios from 'axios'
import {flowRight as compose} from 'lodash' 
import {
	getRoles, 
	getPaymentmodes, 
	getRooms,
	getUsers
} from '../graphql/queries'
import {
	roomDeleteMutation,
	roleDeleteMutation, 
	paymentDeleteMutation,
	userUpdateMutation, 
	userDeleteMutation
} from '../graphql/mutations'



const AdminPage = (props)=>{

	// for pop-up message for Successful or failure
	const Toast = Swal.mixin({
	 	toast: true,
		position: 'top-end',
		showConfirmButton: false,
		timer: 3000,
		timerProgressBar: true,
		onOpen: (toast) => {
			toast.addEventListener('mouseenter', Swal.stopTimer)
			toast.addEventListener('mouseleave', Swal.resumeTimer)
		}
	})


	const name = localStorage.getItem('firstname');
	const token = localStorage.getItem('token');
	const role = localStorage.getItem('role');
	if(name == null && token == null && role == null){
		return(
			window.location.href = './login'
		)
	}
	else{
		if(role === "Customer"){
			return(
				window.location.href = './customer'
			)
		}
		else if(role === "Cashier"){
			return(
				window.location.href = './cashier'
			)
		}
		else if(role === "null"){
			return(
				localStorage.clear(),
				window.location.href = './login'
			)
		}
	}

	const paymentmodes = props.getPaymentmodes.paymentmodes
	const rooms = props.getRooms.rooms
	const roles = props.getRoles.roles
	const users = props.getUsers.users

	const deleteRole = (id)=>{
		props.roleDeleteMutation({
			variables: {id: id},
			refetchQueries:[{query:getRoles}]
		}).then((response)=>{
			const roleDeleted = response.data.deleteRole

			if (roleDeleted) {
				Toast.fire({
	                title: 'Successfully deleted',
	                icon: 'success'
	            })
			}
			else{
				Toast.fire({
                    title: 'Failed to delete data',
                    icon: 'error'
                })
			}
		})
	}
	
	const deletePaymentmode = (id)=>{
		props.paymentDeleteMutation({
			variables: {id: id},
			refetchQueries:[{query:getPaymentmodes}]
		}).then((response)=>{
			const modeDeleted = response.data.deletePaymentmode

			if (modeDeleted) {
				Toast.fire({
	                title: 'Successfully deleted',
	                icon: 'success'
	            })
			}
			else{
				Toast.fire({
                    title: 'Failed to delete data',
                    icon: 'error'
                })
			}
		})
	}
	// delete user
	const deleteUser = (id)=>{
		props.userDeleteMutation({
			variables: {id: id},
			refetchQueries: [{query:getUsers}]
		}).then((response)=>{
			const userDeleted = response.data.deleteUser

			if (userDeleted) {
				Toast.fire({
	                title: 'Successfully deleted',
	                icon: 'success'
	            })
			}
			else{
				Toast.fire({
                    title: 'Failed to delete data',
                    icon: 'error'
                })
			}
		})
	}

	const deleteRoom = (id)=>{
		props.roomDeleteMutation({
			variables: {id: id},
			refetchQueries:[{query:getRooms}]
		}).then((response)=>{
			const roomDeleted = response.data.deleteRoom
			if (roomDeleted) {
				Toast.fire({
	                title: 'Successfully deleted',
	                icon: 'success'
	            })
			}
			else{
				Toast.fire({
                    title: 'Failed to delete data',
                    icon: 'error'
                })
			}
		})
	}
	
	return(
		<React.Fragment>
			<div className="container-fluid" id="admin-container">
				<div className="row" id="admin-row">
					<div className="col-lg-4 col-md-4 col-xs-12" id="admin-small-col">
						<div className="admin-box" id="admin-prof">
							<div className="admin-img-box">
								<i className="fas fa-user"></i>
							</div>
							<div id="admin-name">{name}</div>
							<div id="admin-role">{role}</div>
						</div>
						
					</div>
					<div className="col-lg-8 col-md-8 col-xs-12" id="admin-large-col">
						<div className="" id="admin-lists">
							<PaymentmodeList paymentmodes = {paymentmodes} deletePaymentmode = {deletePaymentmode} roles = {roles} deleteRole = {deleteRole}/>
							<RoomList rooms = {rooms} deleteRoom = {deleteRoom}/>
							<UserList roles = {roles} users = {users} deleteUser = {deleteUser}/>
						</div>
					</div>
				</div>
			</div>
			<Footer/>
		</React.Fragment>
	)
}
export default compose(
	graphql(getRooms, {name: 'getRooms'}),
	graphql(roomDeleteMutation, {name: 'roomDeleteMutation'}),

	graphql(getUsers, {name: 'getUsers'}),
	graphql(userUpdateMutation, {name: 'userUpdateMutation'}),
	graphql(userDeleteMutation, {name: 'userDeleteMutation'}),

	graphql(getRoles, {name: 'getRoles'}),
	graphql(roleDeleteMutation, {name: 'roleDeleteMutation'}),

	graphql(getPaymentmodes, {name: 'getPaymentmodes'}),
	graphql(paymentDeleteMutation, {name: 'paymentDeleteMutation'}),
)(AdminPage)
