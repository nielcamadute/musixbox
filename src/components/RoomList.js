import React, {useState} from 'react';
// import {Link} from 'react-router-dom'
import {graphql} from 'react-apollo';
import {flowRight as compose} from 'lodash' 
import Swal from 'sweetalert2'
import axios from 'axios'
import {getRooms} from '../graphql/queries'
import {roomAddMutation, roomUpdateMutation} from '../graphql/mutations'

const RoomList = (props)=>{
	const rooms = props.rooms
	const deleteRoom = props.deleteRoom

	// counter for error
	let countError = 0

	// for pop-up message for Successful or failure
	const Toast = Swal.mixin({
	 	toast: true,
		position: 'top-end',
		showConfirmButton: false,
		timer: 3000,
		timerProgressBar: true,
		onOpen: (toast) => {
			toast.addEventListener('mouseenter', Swal.stopTimer)
			toast.addEventListener('mouseleave', Swal.resumeTimer)
		}
	})

	// function to upload image and directly save to cloudinary.com
	const [image, setImage] = useState('');
	const [loading, setLoading] = useState(false);
	const uploadImage = e =>{
		const files = e.target.files[0];
		const formData = new FormData();
		formData.append("upload_preset", "booking");
		formData.append("file", files);
		setLoading(true);

		axios.post('https://api.cloudinary.com/v1_1/dyc8nolbm/image/upload', formData)
		.then(res=>setImage(res.data.secure_url))
		.then(setLoading(false))
		.catch(err => console.log(err))
	}


	// add function and usestates
	const [room, setroom] = useState('')
	const [roomsize, setroomsize] = useState('')
	const [capacity, setcapacity] = useState('')
	const newRoom = {
		room: room,
		roomsize: roomsize,
		capacity: parseInt(capacity),
		imageLocation: image
	}
	const addRoom = (e, newRoom)=>{
		e.preventDefault()
		let checkroom = room.toLowerCase();

		// to check if empty or not
		if(room === ""){
			Toast.fire({ title: 'Room field is empty', icon: 'error'})
			countError += 1
		}
		if(roomsize === ""){
			Toast.fire({ title: 'Room Size field is empty', icon: 'error'})
			countError += 1
		}
		if(capacity === ""){
			Toast.fire({ title: 'Capacity field is empty', icon: 'error'})
			countError += 1
		}
		if(image === ""){
			Toast.fire({ title: 'Image is missing', icon: 'error'})
			countError += 1
		}

		// to check if the payment mode is available or not
		let roomlist
		if (rooms !== undefined) {
			roomlist = rooms.map(r =>{
				const{room} = r

				// convert data to lowercase for checking
				let lcaseroom = room.toLowerCase()

				if(lcaseroom === checkroom){
					Toast.fire({ title: 'Mode already exist', icon: 'error'})
					countError += 1
				}
			})
		}

		if(countError === 0){
			props.roomAddMutation({
				variables: newRoom,
				refetchQueries:[{query:getRooms}],
			}).then((response)=>{
				const roomAdded = response.data.addRoom

				if (roomAdded) {
					Swal.fire({
		                title: 'Successfully added',
		                type: 'success'
		            }).then(()=>{
		            	setroom('')
		            	setroomsize('')
		            	setcapacity('')
		            	setImage('')
		            	setLoading(false)
		            })
				}
				else{
					Swal.fire({
	                    title: 'Failed to add data',
	                    type: 'error'
	                })
				}
			})
		}
	}

	// use to delete/remove the copied room data in localstorage
	let deleteRoomCopy = ()=>{
		localStorage.removeItem('getroomid')
		localStorage.removeItem('getroom')
		localStorage.removeItem('getroomsize')
		localStorage.removeItem('getcapacity')
		localStorage.removeItem('getimage')
		setImage('')
	}

	//usestate use to retrieve temporary room data to update
	const[roomValueId, setroomValueId] = useState('')
	const[roomValue, setroomValue] = useState('')
	const[roomsizeValue, setroomsizeValue] = useState('')
	const[capacityValue, setcapacityValue] = useState('')
	const[imageValue, setimageValue] = useState('')


	// pass final data for image location
	let imagefinal
	image ? imagefinal = image : imagefinal = imageValue

	// update function for room
	const roomUpd = {
		id: roomValueId,
		room: roomValue,
		roomsize: roomsizeValue,
		capacity: parseInt(capacityValue),
		imageLocation: imagefinal,
	}
	const updateRoom = (e, roomUpd)=>{
		e.preventDefault()
		props.roomUpdateMutation({
			variables: roomUpd
		}).then(()=>{
			deleteRoomCopy()
			// window.location.href = './admin'
			window.location.reload()
		})
	}


	// list of rooms
	let list = <li className="ring ring-margin">Loading</li>
	if (rooms !== undefined) {
		list = rooms.map((rom)=>{
			const {id, room, roomsize, capacity} = rom

			// temporarily save/copy room data to localstage for update purposes
			let roomCopy = ()=>{
				localStorage.setItem('getroomid', rom.id)
				localStorage.setItem('getroom', rom.room)
				localStorage.setItem('getroomsize', rom.roomsize)
				localStorage.setItem('getcapacity', rom.capacity)
				localStorage.setItem('getimage', rom.imageLocation)
				setroomValueId(localStorage.getItem('getroomid'))
				setroomValue(localStorage.getItem('getroom'))
				setroomsizeValue(localStorage.getItem('getroomsize'))
				setcapacityValue(localStorage.getItem('getcapacity'))
				setimageValue(localStorage.getItem('getimage'))
			}
			return(
				<li key={id}>
		            <span>{room}</span>
		            <span className="mobile-hide">{roomsize}</span>
		            <span className="mobile-hide">{capacity}</span>
		            <span className="btn-group mobile-adjust" role="group">
		            	<button className="btn btn-item" data-toggle="modal" data-target="#modal-room-update" onClick={()=>roomCopy()}>
		            		<i className="far fa-edit"></i>
		            	</button>
		            	<button className="btn btn-item" onClick={()=>deleteRoom(id)}>
		            		<i className="far fa-trash-alt"></i>
		            	</button>
		            </span>
		        </li>
			)
		})
	}

	return(
		<div className="row row-room" id="room-row">
		    <div className="col-lg-12 col-md-12 col-xs-12">
			    <ul className="room-ul">
			    	<div className="room-div">
						<span id="room-span-header"><h3>Room</h3></span>
						<span id="room-span-add"></span>
					</div>
			    	<li id="room-list-label">
			            <span>Name</span>
			            <span className="mobile-hide">Size</span>
			            <span className="mobile-hide">Capacity</span>
			            <span>
			            	<a type="button" className="mobile-adjust" data-toggle="modal" data-target="#modal-room">
								<i className="fas fa-plus"></i>
							</a>
			            </span>
			        </li>
					{list}

					<React.Fragment>
						<div className="modal fade" tabIndex="-1" id="modal-room">
							<div className="modal-dialog">
								<div className="modal-content">
									<div className="modal-header room-modal-header" id="">
										<h6 className="modal-title">Add New Room</h6>
										<button type="button" className="close" data-dismiss="modal">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div className="modal-body">
								    	<form action="" className="form room-form" onSubmit={(e)=>addRoom(e,newRoom)} id="room-add">
										    <div className="form-group">
										        <label htmlFor="room">Room</label>
										        <input type="text" className="form-control" id="room" name="room" value={room} onChange={(e)=> setroom(e.target.value)} required/>
										    </div>
										    <div className="form-group">
										        <label htmlFor="roomsize">Room Size</label>
										        <input type="text" className="form-control" id="roomsize" name="roomsize" value={roomsize} onChange={(e)=> setroomsize(e.target.value)} required/>
										    </div>
										    <div className="form-group">
										        <label htmlFor="capacity">Capacity</label>
										        <input type="number" className="form-control" id="capacity" name="capacity" value={capacity} onChange={(e)=> setcapacity(e.target.value)} required/>
										    </div>

										    <label htmlFor="customFile">Image</label>
										    <div className='custom-file mb-4'>
									          <input
									            type='file'
									            className='custom-file-input'
									            name = "file"
									            id='customFile'
									            onChange={uploadImage}
									          />
									          <label className='custom-file-label image-label' htmlFor='customFile' id="">
									            {image}
									          </label>
									        </div>
									        <div className="img-preview-div">
									          {loading ? <span>loading...</span> : <img className = "img-fluid" src={image} id="room-image" alt="i"/>}
									        </div>
										    <button type="submit" className="btn" id="room-modal-submit">Submit</button>
										</form>

									</div>	
								</div>
							</div>
						</div>

						<div className="modal fade" tabIndex="-1" id="modal-room-update">
							<div className="modal-dialog">
								<div className="modal-content">
									<div className="modal-header room-modal-header" id="">
										<h6 className="modal-title">Update Room Record</h6>
										<button type="button" className="close" data-dismiss="modal" onClick={()=>deleteRoomCopy()}>
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div className="modal-body">
								    	<form action="" className="form room-form" onSubmit={(e)=>updateRoom(e,roomUpd)} id="room-update">
										    <div className="form-group" hidden>
										        <label htmlFor="room">Room Id</label>
										        <input type="text" className="form-control" id="updateRoomId" name="updateRoomId" value={roomValueId} onChange={(e)=> setroomValueId(e.target.value)} required/>
										    </div>
										    <div className="form-group">
										        <label htmlFor="room">Room</label>
										        <input type="text" className="form-control" id="updateRoom" name="updateRoom" value={roomValue} onChange={(e)=> setroomValue(e.target.value)} required/>
										    </div>
										    <div className="form-group">
										        <label htmlFor="roomsize">Room Size</label>
										        <input type="text" className="form-control" id="updateRoomsize" name="updateRoomsize" value={roomsizeValue} onChange={(e)=> setroomsizeValue(e.target.value)} required/>
										    </div>
										    <div className="form-group">
										        <label htmlFor="capacity">Capacity</label>
										        <input type="number" className="form-control" id="updateCapacity" name="updateCapacity" value={capacityValue} onChange={(e)=> setcapacityValue(e.target.value)} required/>
										    </div>

										    <label htmlFor="updatecustomFile">Image</label>
										    <div className='custom-file mb-4'>
									          <input
									            type='file'
									            className='custom-file-input'
									            name = "updatefile"
									            id='updatecustomFile'
									            onChange={uploadImage}
									          />
									          <label className='custom-file-label image-label' htmlFor='updatecustomFile' id="">
									            {image}
									          </label>
									        </div>
									        <div className="img-preview-div">
									          <img className = "img-fluid" src={imagefinal} id="room-image-update"/>
									        </div>
										    <button type="submit" className="btn" id="room-modal-update">Update</button>
										</form>
									</div>	
								</div>
							</div>
						</div>
					</React.Fragment>
				</ul>
		    </div>
		</div>
	)
}
export default compose(
	graphql(roomAddMutation, {name: 'roomAddMutation'}),
	graphql(roomUpdateMutation, {name: 'roomUpdateMutation'}),
)(RoomList)



