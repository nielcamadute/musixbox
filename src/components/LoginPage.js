import React, {useState} from 'react'
/*Sweet alert*/
import Swal from 'sweetalert2'
/*We'll use Redirect, to redirect the user into the homepage after successfully LoggedIn*/
import { Redirect, Link } from 'react-router-dom'
/*Mutations for Log in*/
import { graphql} from 'react-apollo'
import { loginMutation } from '../graphql/mutations'
import Footer from './Footer';

// import {Redirect} from "react-router-dom"

const LoginPage = (props)=>{
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	// state that will check whether to redirect or not.
	const [isRedirected, setIsRedirected] = useState(false)

	

	// for pop-up message for Successful or failure
	const Toast = Swal.mixin({
	 	toast: true,
		position: 'top-end',
		showConfirmButton: false,
		timer: 3000,
		timerProgressBar: true,
		onOpen: (toast) => {
			toast.addEventListener('mouseenter', Swal.stopTimer)
			toast.addEventListener('mouseleave', Swal.resumeTimer)
		}
	})

	const namename = localStorage.getItem('firstname');
	const tokentoken = localStorage.getItem('token');
	const rolerole = localStorage.getItem('role');

	if(namename !== null && tokentoken !== null && rolerole !== null){
		if(rolerole === 'Customer'){
			return(
				window.location.href = './customer'
			)
		}
		else if(rolerole === "Cashier"){
			return(
				window.location.href = './cashier'
			)
		}
		else if(rolerole === "Manager"){
			return(
				window.location.href = './admin'
			)
		}
		else if(rolerole === "null"){
			return(
				localStorage.clear(),
				window.location.href = './login'
			)
		}
	}





	/*
		STEP 2.
		Create the states for the email and password
		Import the state from react module
	*/

	

	if (isRedirected) {
		const role = localStorage.getItem('role');
		let redirect;

		switch(role){
			case "Customer":
				redirect = window.location.href = './customer' ;
				// redirect = <Redirect to='/customer'/>;
				break;
			case "Manager":
				redirect = window.location.href = './admin' ;
				break;
			case "Cashier":
				redirect = window.location.href = './cashier' ;
				break;
			default:
				redirect = window.location.href = './' ;
				break;
		}
        return redirect
    }

	/*
		STEP 3.
		Create a function that will Authenticate a user
	*/

	const login = (e) => {
		e.preventDefault()
		/*
			STEP 5.
			import the loginMutation, use it for the user Login. 
			export the mutation using graphql()
			Pass the states on the variables property
		*/

		props.loginMutation({
            variables: {
                email: email,
                password: password
            }
        }).then((response) => {
            // console.log(response)
            /*
				STEP 6.
				We'll save our credentials to localStorage. 
				After the loginMutation successfully authenticates our credentials, we get our data from the response and store it to localStorage. This will serve as our 'session'.
            */
            let data = response.data.login

            if (data != null) {
	   			Toast.fire({
                    title: 'Login successfully!',
                    icon: 'success'
                }).then(()=>{
	                localStorage.setItem('userId', data.id)
	                localStorage.setItem('firstname', data.firstname)
	                localStorage.setItem('role', data.role)
	                localStorage.setItem('token', data.token)
		            // console.log(localStorage)
		   			//we will be redirected to Products Page
		            setIsRedirected(true)

                })
            } else {
                Toast.fire({
                    title: 'Login Failed',
                    text: 'Either your email or password is incorrect, please try again.',
                    icon: 'error'
                })
            }
        })
	}


	/*
		STEP 1.
		Create the login form
		Make sure to import all the modules and necessary components that we need for login page
	*/

	/*
		STEP 4. 
		Apply the login() function the form onSubmit event
		Apply the states on the input fields

	*/
	
	return(
		<React.Fragment>
			<div className="container-fluid" id="login-container">
				<div className="row row-page" id="row-login">
					<div className="col-lg-6 col-md-12 col-xs-12 login-col">
					</div>
					<div className="col-lg-6 col-md-12 col-xs-12 login-col">
						<form onSubmit={(e)=> login(e)} id="form-login">
							<h1>Login</h1>
							<div className="form-group">
								<label htmlFor="fullname">Email</label>
								<input 
									value={email}
									onChange={(e)=> setEmail(e.target.value)}
									type="email" 
									name="fullname"
									id="fullname"
									className="form-control"/>
							</div>
							<div className="form-group">
								<label htmlFor="password">Password</label>
								<input 
									value={password}
									onChange={(e)=> setPassword(e.target.value)}
									type="password" 
									name="password"
									id="password"
									className="form-control"/>
							</div>
							<div className="form-group">
								<button className="btn">Login</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<Footer/>
		</React.Fragment>
	)
}


export default graphql(loginMutation, {name: 'loginMutation'})(LoginPage)