
import React, {useState} from 'react';
// import {Link} from 'react-router-dom'
// import BookingPage from './BookingPage'
import {graphql} from 'react-apollo';
import {flowRight as compose} from 'lodash' 
import {getRooms} from '../graphql/queries'

const Catalog = (props)=>{


	const [roomType, setroomType] = useState("")
	const role = localStorage.getItem('role');

	if(role === 'Cashier'){
		return(
			window.location.href = './cashier'
		)
	}
	else if(role === "Manager"){
		return(
			window.location.href = './admin'
		)
	}
	else if(role === "null"){
		return(
			localStorage.clear(),
			window.location.href = './login'
		)
	}

	const rooms =  props.getRooms.rooms
	
	let list = <div className="col-lg-12 col-md-12 col-xs-12" id="loading-col">
					<div className="loading">
	                    <h2 id="loading-text">loading</h2>
					</div>
                </div>
// data-toggle="modal" data-target="#modal-customer-booking"

	let cancelBooking = ()=>{
		localStorage.removeItem('roomid')
		localStorage.removeItem('room')
		localStorage.removeItem('roomcapacity')
		document.querySelector(".cat-notif").style.visibility= "hidden"
		document.querySelector(".navbar").style.visibility= "visible"
		document.body.style.overflow = "visible"
		document.body.style.scroll = "yes"
		// window.location.href = './catalog'
	}

	let proceedBooking = ()=>{
		if(!localStorage.getItem('userId') && !localStorage.getItem('role')){
			window.location.href = './login'
		}
		if(localStorage.getItem('userId') && localStorage.getItem('role')){
			window.location.href = './customer'
		}
	}


	if (rooms !== undefined) {
		list = rooms.map((rom)=>{

			let copyRoomData = ()=>{
				localStorage.setItem('roomid', rom.id)
				localStorage.setItem('room', rom.room)
				localStorage.setItem('roomcapacity', rom.capacity)
				document.querySelector(".cat-notif").style.visibility= "visible"
				document.querySelector(".navbar").style.visibility= "hidden"
				document.body.style.overflow = "hidden"
				document.body.style.scroll = "no"
			}
			const {id, room, capacity, imageLocation} = rom
			return(
				<React.Fragment key={id}>
					<div className="col-lg-4 col-md-6 col-xs-12" >
						<img className="cart-image img-fluid" src={imageLocation} alt=""/>
						<div className="cart-label">
							<h3>{room}</h3>				
							<div>{capacity} pax (max)</div>
							<div className="cart-button">
				            	<button 
				            		className="btn cart-btn" 
				            		type="submit" 
				            		onClick = {()=>copyRoomData()}
				            		>
				            		Book
				            	</button>
							</div>
						</div>
					</div>
					<div className="cat-notif">
						<div className="cat-notif-msg">
							<h5>Are you sure do you want to book this room?</h5>
							<button className="btn btn-danger" onClick = {()=>cancelBooking()}>Cancel</button>
							<button className="btn btn-primary" onClick = {()=>proceedBooking()}>Yes, proceed</button>
						</div>
					</div>
				</React.Fragment>
			)
		})
	}

	return(
		<div className="container-fluid cat-con" id="catalog-container">
			<div className="row">
				<div className="col-lg-12 col-md-12 col-xs-12">
                    <h1>Catalog</h1>
                </div>

				{list}
			</div>
		</div>
	)
}
export default compose(
	graphql(getRooms, {name: 'getRooms'}),
)(Catalog)
