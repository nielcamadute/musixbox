import React, {useState} from 'react';
import Swal from 'sweetalert2'
import {graphql} from 'react-apollo';
import {flowRight as compose} from 'lodash' 
import {getBookings, getUsers} from '../graphql/queries'
import {userUpdateMutation} from '../graphql/mutations'
import Footer from './Footer';

// declaration use for encryption
const bcrypt = require('bcryptjs')
const BookingPage = (props)=>{
	// for pop-up message for Successful or failure
	const Toast = Swal.mixin({
	 	toast: true,
		position: 'top-end',
		showConfirmButton: false,
		timer: 3000,
		timerProgressBar: true,
		onOpen: (toast) => {
			toast.addEventListener('mouseenter', Swal.stopTimer)
			toast.addEventListener('mouseleave', Swal.resumeTimer)
		}
	})


	// usestate for customer's data, use for update
	const [custid, setcustid] = useState(localStorage.getItem("userId"))
	const [custfirstname, setcustfirstname] = useState('')
	const [custlastname, setcustlastname] = useState('')
	const [custemail, setcustemail] = useState('')
	const [custpassword, setcustpassword] = useState('')
	const [custcontact, setcustcontact] = useState('')
	const [custgender, setcustgender] = useState('')
	const [custaddress, setcustaddress] = useState('')
	const [custrole, setcustrole] = useState('')
	const [custusername, setcustusername] = useState('')


	const userIdLocal = localStorage.getItem('userId');
	const name = localStorage.getItem('firstname');
	const token = localStorage.getItem('token');
	const role = localStorage.getItem('role');
	if(name == null && token == null && role == null){
		return(
			window.location.href = './login'
		)
	}
	else{
		if(role === "Cashier"){
			return(
				window.location.href = './cashier'
			)
		}
		else if(role === "Manager"){
			return(
				window.location.href = './admin'
			)
		}
		else if(role === "null"){
			return(
				localStorage.clear(),
				window.location.href = './login'
			)
		}
	}







	let list = <li className="ring">Loading</li>
	const bookings = props.getBookings.bookings
	if (bookings !== undefined) {
		list = bookings.map((booking)=>{
			const {userId, id, roomType, bookDate, bookTime, bookstatus} = booking
			if(userIdLocal === userId){
				return(
					<li className="history-list" key={id}>
						<span>{roomType}</span>
						<span>{bookDate}</span>
						<span>{bookTime}</span>
						<span>{bookstatus}</span>
					</li>
				)
			}
		})
	}


	let copyCustomerData = ()=>{
		const userlist = props.getUsers.users
		let getUserData
		if(userlist !== undefined){
			getUserData = userlist.map((user)=>{
				if(localStorage.getItem("userId") === user.id){
					localStorage.setItem("custfirstname", user.firstname)
					localStorage.setItem("custlastname", user.lastname)
					localStorage.setItem("custemail", user.email)
					localStorage.setItem("custpassword", user.password)
					localStorage.setItem("custcontact", user.contact)
					localStorage.setItem("custaddress", user.address)
					localStorage.setItem("custrole", user.role)
					localStorage.setItem("custusername", user.username)
					localStorage.setItem("custgender", user.gender)
					setcustfirstname(localStorage.getItem('custfirstname'))
					setcustlastname(localStorage.getItem('custlastname'))
					setcustemail(localStorage.getItem('custemail'))
					setcustpassword(localStorage.getItem('custpassword'))
					setcustcontact(localStorage.getItem('custcontact'))
					setcustaddress(localStorage.getItem('custaddress'))
					setcustrole(localStorage.getItem('custrole'))
					setcustusername(localStorage.getItem('custusername'))
					setcustgender(localStorage.getItem('custgender'))
				}
			})
		}
	}

	let deleteCopiedCustomerData = ()=>{
		localStorage.removeItem("custfirstname")
		localStorage.removeItem("custlastname")
		localStorage.removeItem("custemail")
		localStorage.removeItem("custpassword")
		localStorage.removeItem("custcontact")
		localStorage.removeItem("custaddress")
		localStorage.removeItem("custrole")
		localStorage.removeItem("custusername")
		localStorage.removeItem("custgender")
	}

	/*to check if the password is old or new; if new, it will be encrypted*/
	let finalcustpassword;
	if(custpassword === localStorage.getItem('custpassword')){
		finalcustpassword = localStorage.getItem('custpassword')
	}
	else{
		// encrypt the password
		finalcustpassword = bcrypt.hashSync(custpassword)
	}


	// update customer information
	const userUpd = {
		id: custid,
		firstname: custfirstname,
		lastname: custlastname,
		email: custemail,
		username: custusername,
		password: finalcustpassword,
		address: custaddress,
		contact: custcontact,
		gender: custgender,
		role: custrole
	}
	const updateCustomer = (e, userUpd)=>{
		e.preventDefault()
		props.userUpdateMutation({
			variables: userUpd
		}).then(()=>{
			Toast.fire({
				icon: 'success',
				title: 'Successfully updated!'
			})

			if(custfirstname !== localStorage.getItem('firstname')){
				localStorage.setItem('firstname', custfirstname)
			}
			deleteCopiedCustomerData()
			// window.location.href = './customer'
			window.location.reload()

		})
	}


	return(
		<React.Fragment>
			<div className="container-fluid" id="customer">
				<div className="row">
					<div className="col-lg-12 col-md-12 col-xs-12" id="customer-profile-col">
						<div id="customer-img-box">
							<i className="fas fa-user"></i>
						</div>
						<div id="customer-name">
							{name}
							<button id="customer-update" data-toggle="modal" data-target="#modal-customer-update" onClick={()=>{copyCustomerData()}}>
								<i className="fas fa-pen-nib"></i>
							</button>
						</div>
						<div id="customer-role">{role}</div>
					</div>
					<div className="col-lg-12 col-md-12 col-xs-12" id="transaction-history-col">
						<div className="modal fade" tabIndex="-1" id="modal-customer-update">
							<div className="modal-dialog modal-md">
								<div className="modal-content">
									<div className="modal-header mode-modal-header" id="">
										<h6 className="modal-title">Update Customer Profile</h6>
										<button type="button" className="close" data-dismiss="modal" onClick={()=>{deleteCopiedCustomerData()}}>
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div className="modal-body">
										<form action="" className="form" id="cashier-form-update" onSubmit={(e)=>updateCustomer(e,userUpd)}>
											<div className="row">
											    <div className="col-lg-12 col-md-12 col-xs-12 col-form-user">
												    <div className="form-group" hidden>
												        <label htmlFor="">User ID</label>
												        <input type="text" className="form-control" id="updateid" name="updatecashid" value={custid} onChange={(e)=> setcustid(e.target.value)} required/>
												    </div>
												    <div className="form-group">
												        <label htmlFor="">First Name</label>
												        <input type="text" className="form-control" id="updatefirstname" name="updatefirstname" value={custfirstname} onChange={(e)=> setcustfirstname(e.target.value)} required/>
												    </div>
												    <div className="form-group">
												        <label htmlFor="">Last Name</label>
												        <input type="text" className="form-control" id="updatelastname" name="updatelastname" value={custlastname} onChange={(e)=> setcustlastname(e.target.value)} required/>
												    </div>
												    <div className="form-group">
												        <label htmlFor="">Email</label>
												        <input type="email" className="form-control" id="updateemail" name="updateemail" value={custemail} onChange={(e)=> setcustemail(e.target.value)} required/>
												    </div>
												    <div className="form-group" hidden>
												        <label htmlFor="">Username</label>
												        <input type="text" className="form-control" id="updateusername" name="updateusername" value={custusername} onChange={(e)=> setcustusername(e.target.value)} required/>
												    </div>
												    <div className="form-group">
												        <label htmlFor="">Password</label>
												        <input type="password" className="form-control" id="updatepassword" name="updatepassword" value={custpassword} onChange={(e)=> setcustpassword(e.target.value)} required/>
												    </div>
												    <div className="form-group" hidden>
												        <label htmlFor="">Confirm Password</label>
												        <input type="password" className="form-control" id="updateconfirm" name="updateconfirm"/>
												    </div>
												    <div className="form-group">
												        <label htmlFor="">Address</label>
												        <input type="text" className="form-control" name="updateaddress" id="updateaddress" value={custaddress} onChange={(e)=> setcustaddress(e.target.value)} required/>

												    </div>
												    <div className="form-group">
												        <label htmlFor="">Contact</label>
												        <input type="text" className="form-control" id="updatecontact" name="updatecontact" value={custcontact} onChange={(e)=> setcustcontact(e.target.value)} required/>
												    </div>
												    <div className="form-group">
												        <label htmlFor="gender">Gender</label>
												        <select className="form-control" name="updategender" id="updategender" value={custgender} onChange={(e)=> setcustgender(e.target.value)} required>
												            <option value="Male">Male</option>
												            <option value="Female">Female</option>
												        </select>
												    </div>
												    <div className="form-group" hidden>
												        <label htmlFor="role">Role</label>
												         <input type="text" className="form-control" name="updaterole" id="updaterole" value={custrole} onChange={(e)=> setcustrole(e.target.value)} readOnly/>
												    </div>
												    <button type="submit" className="btn" id="cashier-modal-update">Update</button>
											    </div>        
											</div>
										</form>
									</div>	
								</div>
							</div>
						</div>
						<h4>Transaction History</h4>
						<ul>
							<li id="history-label">
								<span>Room</span>
								<span>Date</span>
								<span>Time</span>
								<span>Status</span>
							</li>
							{list}
						</ul>
					</div>

				</div>
			</div>
			<Footer/>
		</React.Fragment>
	)
}
export default compose(
	graphql(getBookings, {name: 'getBookings'}),
	graphql(getUsers, {name: 'getUsers'}),
	graphql(userUpdateMutation, {name: 'userUpdateMutation'}),
)(BookingPage)
