import React, {useState} from 'react';
import {Redirect} from 'react-router-dom'
import Swal from 'sweetalert2'
import {flowRight as compose} from 'lodash'
import {graphql} from 'react-apollo'
import {userAddMutation} from '../graphql/mutations'
import {getUsers} from '../graphql/queries'
import Footer from './Footer';


const Register = (props)=>{
	
	const [firstname, setfirstname] = useState('');
	const [lastname, setlastname] = useState('');
	const [email, setemail] = useState('');
	const [username, setusername] = useState('');
	const [password, setpassword] = useState('');
	const [confirm, setconfirm] = useState('');
	const [address, setaddress] = useState('');
	const [contact, setcontact] = useState('');
	const [gender, setgender] = useState('');
	// const [role, setrole] = useState('Customer');
	const [redirectToLogin, setRedirectToLogin] = useState(false)
	
	const namename = localStorage.getItem('firstname');
	const tokentoken = localStorage.getItem('token');
	const rolerole = localStorage.getItem('role');

	if(namename !== null && tokentoken !== null && rolerole !== null){
		if(rolerole === 'Customer'){
			return(
				window.location.href = './customer'
			)
		}
		else if(rolerole === "Cashier"){
			return(
				window.location.href = './cashier'
			)
		}
		else if(rolerole === "Manager"){
			return(
				window.location.href = './admin'
			)
		}
		else if(rolerole === "null"){
			return(
				localStorage.clear(),
				window.location.href = './login'
			)
		}
	}
	

	if (redirectToLogin) {
		return <Redirect to='login?register=true'/>
	}

	// counter for error
	let countError = 0

	// for pop-up message for Successful or failure
	const Toast = Swal.mixin({
	 	toast: true,
		position: 'top-end',
		showConfirmButton: false,
		timer: 3000,
		timerProgressBar: true,
		onOpen: (toast) => {
			toast.addEventListener('mouseenter', Swal.stopTimer)
			toast.addEventListener('mouseleave', Swal.resumeTimer)
		}
	})


	const newUser = {
		firstname: firstname,
		lastname: lastname,
		email: email,
		username: username,
		password: password,
		address: address,
		contact: contact,
		gender: gender,
		role: 'Customer'
	}
	const addUser = (e, newUser)=>{
		// preventing the form from loading
		e.preventDefault()
		const users = props.getUsers.users
		let checkregemail = email.toLowerCase();
		let reglist;

		// to check if empty or not
		if(firstname === ""){
			Toast.fire({ title: 'Firstname field is empty', icon: 'error'})
			countError += 1
		}
		if(lastname === ""){
			Toast.fire({ title: 'Lastname field is empty', icon: 'error'})
			countError += 1
		}
		if(email === ""){
			Toast.fire({ title: 'Email field is empty', icon: 'error'})
			countError += 1
		}
		if(username === ""){
			Toast.fire({ title: 'Username field is empty', icon: 'error'})
			countError += 1
		}
		if(password === ""){
			Toast.fire({ title: 'Password field is empty', icon: 'error'})
			countError += 1
		}
		if(address === ""){
			Toast.fire({ title: ' field is empty', icon: 'error'})
			countError += 1
		}
		if(contact === ""){
			Toast.fire({ title: 'Contact field is empty', icon: 'error'})
			countError += 1
		}
		if(gender === ""){
			Toast.fire({ title: 'Gender field is empty', icon: 'error'})
			countError += 1
		}

		if (users !== undefined) {
			reglist = users.map(u =>{
				const{email} = u

				// convert data to lowercase for checking
				let lcaseemail = email.toLowerCase()

				if(lcaseemail === checkregemail){
					Toast.fire({ title: 'Email Address is already used', icon: 'error'})
					countError += 1
				}
			})
		}

		if(countError === 0){
			props.userAddMutation({
				variables: newUser
			}).then((response)=>{
				const userAdded = response.data.addUser

				if (userAdded) {
					Swal.fire({
		                title: 'Registration Successful',
		                text: 'You will now be redirected to the login.',
		                type: 'success'
		            }).then(() => {
		                setRedirectToLogin(true)
		            })
				}
				else{
					Swal.fire({
	                    title: 'Registration Failed',
	                    text: 'The server encountered an error.',
	                    type: 'error'
	                })
				}
			})
		}
	}





	return(
		<React.Fragment>
			<div className="container-fluid" id="register-container">
				<div className="row row-page" id="row-register">
				    <div className="col-lg-12 col-md-12 col-xs-12">        
						<form className="form" onSubmit={(e)=>addUser(e,newUser)} id="form-register">
							<div className="row">
							    <div className="col-lg-12 col-md-12 col-xs-12">
									<h1>Register</h1>
							    </div>
							    <div className="col-lg-6 col-md-12 col-xs-12 col-form-register" id="half1">
								    <div className="form-group">
								        <label htmlFor="">First Name</label>
								        <input type="text" className="form-control" id="firstname" name="firstname" value={firstname} onChange={(e)=> setfirstname(e.target.value)} required/>
								    </div>
								    <div className="form-group">
								        <label htmlFor="">Last Name</label>
								        <input type="text" className="form-control" id="lastname" name="lastname" value={lastname} onChange={(e)=> setlastname(e.target.value)} required/>
								    </div>
								    <div className="form-group">
								        <label htmlFor="">Email</label>
								        <input type="email" className="form-control" id="email" name="email" value={email} onChange={(e)=> setemail(e.target.value)} required/>
								    </div>
								    <div className="form-group">
								        <label htmlFor="">Username</label>
								        <input type="text" className="form-control" id="username" name="username" value={username} onChange={(e)=> setusername(e.target.value)} required/>
								    </div>
							    </div>        
							    <div className="col-lg-6 col-md-12 col-xs-12 col-form-register" id="half2">
								    <div className="form-group">
								        <label htmlFor="">Password</label>
								        <input type="password" className="form-control" id="password" name="password" value={password} onChange={(e)=> setpassword(e.target.value)} required/>
								    </div>
								    <div className="form-group" hidden>
								        <label htmlFor="">Confirm Password</label>
								        <input type="password" className="form-control" id="confirm" name="confirm" value={confirm} onChange={(e)=> setconfirm(e.target.value)}/>
								    </div>
								    <div className="form-group">
								        <label htmlFor="">Address</label>
								        <input type="text" className="form-control" name="address" id="address" value={address} onChange={(e)=> setaddress(e.target.value)}  required/>
								    </div>
								    <div className="form-group">
								        <label htmlFor="">Contact</label>
								        <input type="text" className="form-control" id="contact" name="contact" value={contact} onChange={(e)=> setcontact(e.target.value)} required/>
								    </div>
								    <div className="form-group">
								        <label htmlFor="gender">Gender</label>
								        <select className="form-control" name="gender" id="gender" value={gender} onChange={(e)=> setgender(e.target.value)} required>
								            <option value="">Select</option>
								            <option value="Male">Male</option>
								            <option value="Female">Female</option>
								        </select>
								    </div>
								    <button type="submit" className="btn">Submit</button>
							    </div>        
							</div>
						</form>
				    </div>
				</div>
			</div>
			<Footer/>
		</React.Fragment>

	)
}

export default compose(
	graphql(getUsers, {name: 'getUsers'}),
	graphql(userAddMutation, {name: 'userAddMutation'}),
)(Register)