import React, {useState} from 'react'
import Swal from 'sweetalert2'
import {Redirect, Link} from "react-router-dom"
import {graphql} from 'react-apollo';
import {flowRight as compose} from 'lodash' 
import {getPaymentmodes, getBookings} from '../graphql/queries'
import {bookingAddMutation} from '../graphql/mutations'

const Navbar = (props)=>{
	// get the data of user in localStorage
	const name = localStorage.getItem('firstname');
	const token = localStorage.getItem('token');
	const role = localStorage.getItem('role');


	const paymentmodes = props.getPaymentmodes.paymentmodes

	// get the current date
	let today = new Date();
	let currentDate = today.getFullYear()+'-'+('0'+(today.getMonth()+1)).slice(-2)+'-'+('0' + today.getDate()).slice(-2);

	// counter for error
	let countError = 0

	// temporary data holder
	let tempUserIdVal = "0"
	let tempUNameVal = "0"
	let tempRoomVal
	// ternary condition for the temporary/final value
	!localStorage.getItem("room")? tempRoomVal = "0" : tempRoomVal = localStorage.getItem("room")
	!localStorage.getItem("firstname")? tempUNameVal = "0" : tempUNameVal = localStorage.getItem("firstname")
	!localStorage.getItem("userId")? tempUserIdVal = "0" : tempUserIdVal = localStorage.getItem("userId")


	// usestate for inout fields 
	const [userId, setuserId] = useState(tempUserIdVal)
	const [uname, setname] = useState(tempUNameVal)
	const [bookDate, setbookDate] = useState()
	const [bookTime, setbookTime] = useState('')
	const [roomType, setroomType] = useState(tempRoomVal)
	const [paymentmode, setpaymentmode] = useState('')
	const [bookstatus, setbookstatus] = useState('Pending')


	// function to remove all items stored in localstorage and to logout
	const clearLogout = ()=>{
		localStorage.clear()
        return (window.location.href = "./login")
	}

	// disable input fields in booking modal
	let disaBookingFields = ()=>{
		if(!localStorage.getItem('role') && !localStorage.getItem('userId'))  {
			document.querySelector('#bookDate').disabled = true
			document.querySelector('#bookTime').disabled = true
			document.querySelector('#paymentmode').disabled = true
			document.querySelector('#bookingsubmit').style.display = "none"
			document.querySelector('#bookingplease').style.display = "block"
			document.querySelector('#bookingplease').style.color = "red"
		}
		if(localStorage.getItem('role') && localStorage.getItem('userId'))  {
			document.querySelector('#bookingplease').style.display = "none"
		}
	}


	// display the booking in navbar once there is a selected room
	let displaybooking = ""
	if(localStorage.getItem("room")){
		displaybooking = (
			<li className="nav-item">
				<a href="#" className="nav-link active" onClick={()=>disaBookingFields()} data-toggle="modal" data-target="#modal-customer-booking" id="navitemBooking">Booking <sup>1</sup> </a>
			</li>
		)
	}
	let logoutlink = (
		<li className="nav-item">
			<a href="./login" className="nav-link active" onClick ={()=>clearLogout()}>Log out</a>
		</li>
	)
	let homecatlink = (
		<React.Fragment>
			<li className="nav-item">
				<a href="./" className="nav-link active">Home</a>
			</li>
			<li className="nav-item">
				<a href="./#catalog-container" className="nav-link active">Catalog</a>
			</li>
		</React.Fragment>
	)
	let regloglink = (
		<React.Fragment>
			<li className="nav-item">
				<a href="./register" className="nav-link active">Register</a>
			</li>
			<li className="nav-item">
				<a href="./login" className="nav-link active">Login</a>
			</li>
		</React.Fragment>
	)



	let displayLink = ''
	if(name !== null && token !== null && role !== null){
		if(role === "Customer"){
			displayLink = (
				<React.Fragment>
					{displaybooking}
					{homecatlink}
					<li className="nav-item">
						<a href="./customer" className="nav-link active">{name}</a>
					</li>
					{logoutlink}
				</React.Fragment>
			)
		}
		if(role === "Manager"){
			displayLink = (
				<React.Fragment>
					<li className="nav-item">
						<a href="./admin" className="nav-link active">{name}</a>
					</li>
					{logoutlink}
				</React.Fragment>
			)
		}
		if(role === "Cashier"){
			displayLink = (
				<React.Fragment>
					<li className="nav-item">
						<a href="./cashier" className="nav-link active">{name}</a>
					</li>
					{logoutlink}
				</React.Fragment>
			)
		}

	}
	else{
		displayLink = (
			<React.Fragment>
				{displaybooking}
				{homecatlink}
				{regloglink}				
			</React.Fragment>
		)
	}

	// for pop-up message for Successful or failure
	const Toast = Swal.mixin({
	 	toast: true,
		position: 'top-end',
		showConfirmButton: false,
		timer: 3000,
		timerProgressBar: true,
		onOpen: (toast) => {
			toast.addEventListener('mouseenter', Swal.stopTimer)
			toast.addEventListener('mouseleave', Swal.resumeTimer)
		}
	})


	// select options for payment mode
	let paymodeOptions = <option>Loading...</option>
	if (paymentmodes !== undefined) {
		paymodeOptions = paymentmodes.map(mode => {
			return(<option key={mode.id}>{mode.mode}</option>)
		})
	}

	const newBooking = {
		userId: userId,
		name: uname,
		bookDate: bookDate,
		bookTime: bookTime,
		roomType: roomType,
		paymentmode: paymentmode,
		bookstatus: bookstatus,
	}
	// validation and add booking
	const addBooking = (e,newBooking)=>{
		e.preventDefault();
		const bookings =  props.getBookings.bookings
		let broomtype = roomType
		let bbookdate = bookDate
		let bbooktime = bookTime
		let blist
		if(userId === "0"){
			Toast.fire({ title: 'Please login first!', icon: 'error'})
			countError += 1
		}
		if(uname === "0"){
			Toast.fire({ title: 'Please login fi!', icon: 'error'})
			countError += 1
		}
		if(roomType === "0"){
			Toast.fire({ title: 'Please select a room in Catalog!', icon: 'error'})
			countError += 1
		}
		if(bookDate < currentDate){
			Toast.fire({ title: 'Please input current/future date!', icon: 'error'})
			countError += 1
		}
		if(!bookDate){
			Toast.fire({ title: 'Booking Date is missing!', icon: 'error'})
			countError += 1
		}
		if(!bookTime){
			Toast.fire({ title: 'Book Time is missing!', icon: 'error'})
			countError += 1
		}
		if(!paymentmode){
			Toast.fire({ title: 'Please select mode of payment!', icon: 'error'})
			countError += 1
		}
		
		// to check if the room is available or not
		if (bookings !== undefined) {
			blist = bookings.map(booking =>{
				const{roomType, bookDate, bookTime} = booking
				if(roomType === broomtype && bookDate === bbookdate && bookTime === bbooktime){
					Toast.fire({ title: 'Room is not available during on given date and time. Pleas select another room/date/time!', icon: 'error'})
					countError += 1
				}
			})
		}

		// if there is no error, data will be added
		if(countError === 0){
			props.bookingAddMutation({
				variables: newBooking
			}).then(()=>{
				Toast.fire({
	                title: 'Book Successfully!',
	                icon: 'success'
	            })
				localStorage.removeItem('room')
				localStorage.removeItem('roomid')
				localStorage.removeItem('roomcapacity')
			}).then(()=>{
				// window.location.href = '/customer'
				window.location.reload()
			})
		}
	}


	// cancel function in booking modal
	const cancelbooking = ()=>{
		localStorage.removeItem('room')
		localStorage.removeItem('roomid')
		localStorage.removeItem('roomcapacity')
		window.location.reload()
	}

	return(
		<div className="nav-section" id="navbar-section">
			<nav className="navbar navbar-expand-md fixed-top" id="navbar">
				<a href="/musixbox" className="navbar-brand">musixb
					<div id="nav-box">
						<div className="navbox-item hidden-xs">
                            <span className="navbox-item-sub">
                                <span className="navbox-triangle"></span>
                            </span>
                        </div>
					</div>
					<p id="nav-box-x">x</p>
				</a>
				<button className="navbar-toggler" data-toggle="collapse" data-target="#nav-dropdown">
					<span className="navbar-toggler-icon"></span>
				</button>

				<div id="nav-dropdown" className="collapse navbar-collapse">
					<ul className="navbar-nav ml-auto">						
						{displayLink}
					</ul>
				</div>
			</nav>
			<div className="modal fade" tabIndex="-1" id="modal-customer-booking">
				<div className="modal-dialog modal-md">
					<div className="modal-content">
						<div className="modal-header mode-modal-header">
							<h6 className="modal-title">Booking</h6>
							<button type="button" className="close" data-dismiss="modal">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div className="modal-body">
							<form action="" className="form" onSubmit={(e)=>addBooking(e, newBooking)}>
								<div className="row">
									<div className="col-lg-12">
									    <div className="form-group" hidden>
									        <label htmlFor="">User Id</label>
									        <input type="text" className="form-control" id="userId" name="userId" value={userId} onChange={(e)=> setuserId(e.target.value)} disabled required/>
									    </div>
									    <div className="form-group" hidden>
									        <label htmlFor="name">Name</label>
									        <input type="text" className="form-control" id="name" name="name" value={uname} onChange={(e)=> setname(e.target.value)} disabled required/>
									    </div>
									    <div className="form-group">
									        <label htmlFor="roomType">Room</label>
									        <input type="text" className="form-control" id="roomType" name="roomType" value={roomType} onChange={(e)=> setroomType(e.target.value)} required disabled/>
									    </div>
									    <div className="form-group">
									        <label htmlFor="bookDate">Book Date</label>
									        <input type="date" className="form-control" id="bookDate" name="bookDate" value={bookDate} onChange={(e)=> setbookDate(e.target.value)} />
									    </div>
									    <div className="form-group">
									        <label htmlFor="bookTime">Book Time</label>
									        <input type="time"className="form-control" id="bookTime" name="bookTime" value={bookTime} onChange={(e)=> setbookTime(e.target.value)} />
									    </div>
									    <div className="form-group">
									        <label htmlFor="paymentmode">Payment Mode</label>
									        <select className="form-control" id="paymentmode" name="paymentmode" value={paymentmode} onChange={(e)=> setpaymentmode(e.target.value)} >
									        	<option></option>
									        	{paymodeOptions}
									        </select>
									    </div>
									    <div className="form-group" hidden>
									        <label htmlFor="bookstatus">Status</label>
									        <input type="text" className="form-control" id="bookstatus" name="bookstatus" placeholder="Pending" value={bookstatus} onChange={(e)=> setbookstatus(e.target.value)} disabled required/>
									    </div>
									    <button type="submit" className="btn btn-danger" data-dismiss="modal" onClick={()=>cancelbooking()}>Cancel</button>
									    <button type="submit" className="btn btn-primary" id="bookingsubmit">Submit</button>
									    <label id="bookingplease">Please login, so you can book the room!</label>
									</div>
								</div>
							</form>
						</div>	
					</div>
				</div>
			</div>
		</div>
	)
}
export default compose(
	graphql(bookingAddMutation, {name: 'bookingAddMutation'}),
	graphql(getPaymentmodes, {name: 'getPaymentmodes'}),
	graphql(getBookings, {name: 'getBookings'}),
)(Navbar)