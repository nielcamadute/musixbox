import React from 'react';


const PageNotFound = ()=>{
	return(
		<div className = "row row-page" id="pagenotfound">
			<div className = "col-lg-12">
				<h1>404</h1>		    
			</div>
		</div>
	)
}

export default PageNotFound