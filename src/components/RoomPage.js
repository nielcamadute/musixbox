import React from 'react'
import RoomList from './RoomList'
import {graphql} from 'react-apollo';
import {flowRight as compose} from 'lodash' 
import {getRooms} from '../graphql/queries'
import {roomDeleteMutation} from '../graphql/mutations'

const RoomPage = (props)=>{
	const rooms =  props.getRooms.rooms



	const deleteRoom = (id)=>{
		props.roomDeleteMutation({
			variables: {id: id},
			refetchQueries:[{query:getRooms}]
		})
	}
	return(
		<React.Fragment>
			<RoomList rooms = {rooms} deleteRoom = {deleteRoom}/>
		</React.Fragment>
	)
}
export default compose(

	graphql(getRooms, {name: 'getRooms'}),
	graphql(roomDeleteMutation, {name: 'roomDeleteMutation'}),
)(RoomPage)
