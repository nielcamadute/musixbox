import React, {useState} from 'react';
import Swal from 'sweetalert2'
import {graphql} from 'react-apollo';
import {flowRight as compose} from 'lodash' 
import {getBookings, getUsers} from '../graphql/queries'
import {bookingUpdateMutation, userUpdateMutation} from '../graphql/mutations'
import Footer from './Footer';


// declaration use for encryption
const bcrypt = require('bcryptjs')
const CashierPage = (props)=>{

	// usestate for cashier data, use for update
	const [cashid, setcashid] = useState(localStorage.getItem("userId"))
	const [cashfirstname, setcashfirstname] = useState('')
	const [cashlastname, setcashlastname] = useState('')
	const [cashemail, setcashemail] = useState('')
	const [cashpassword, setcashpassword] = useState('')
	const [cashcontact, setcashcontact] = useState('')
	const [cashgender, setcashgender] = useState('')
	const [cashaddress, setcashaddress] = useState('')
	const [cashrole, setcashrole] = useState('')
	const [cashusername, setcashusername] = useState('')

	// usestate for booking data, use for update
	const [bookid, setbookid] = useState('')
	const [bookuserid, setbookuserid] = useState('')
	const [bookname, setbookname] = useState('')
	const [booktype, setbooktype] = useState('')
	const [bookdate, setbookdate] = useState('')
	const [booktime, setbooktime] = useState('')
	const [bookmode, setbookmode] = useState('')
	const [bookstatus, setbookstatus] = useState('')
	const [bookapproveby, setbookapproveby] = useState('')



	// some info of user, and use to identify what type of user to prevent accessing the probihited pages
	const name = localStorage.getItem('firstname');
	const token = localStorage.getItem('token');
	const role = localStorage.getItem('role');
	if(name == null && token == null && role == null){
		return(
			window.location.href = './login'
		)
	}
	else{
		if(role === 'Customer'){
			return(
				window.location.href = './customer'
			)
		}
		else if(role === "Manager"){
			return(
				window.location.href = './admin'
			)
		}
		else if(role === "null"){
			return(
				localStorage.clear(),
				window.location.href = './login'
			)
		}
	}

	// hide the option based on the booking status
	const hideStatusOptions = ()=>{
		if(localStorage.getItem("getbookstatus") === "Pending"){
			document.querySelector("#optioncheckin").style.display = "none"
			document.querySelector("#optionpending").style.display = "block"
			document.querySelector("#bookstatus").style.border = "1px solid red"
		}
		if(localStorage.getItem("getbookstatus") === "Approved"){
			document.querySelector("#optionpending").style.display = "none"
			document.querySelector("#optioncheckin").style.display = "block"
			document.querySelector("#bookstatus").style.border = "1px solid #311b92"
		}
	}



	// for pop-up message for Successful or failure
	const Toast = Swal.mixin({
	 	toast: true,
		position: 'top-end',
		showConfirmButton: false,
		timer: 3000,
		timerProgressBar: true,
		onOpen: (toast) => {
			toast.addEventListener('mouseenter', Swal.stopTimer)
			toast.addEventListener('mouseleave', Swal.resumeTimer)
		}
	})

	// delete function for temporarily copied booking data for status update
	let deleteCopiedBookData = ()=>{
		localStorage.removeItem("getbookid");
		localStorage.removeItem("getbookuserid");
		localStorage.removeItem("getbookname");
		localStorage.removeItem("getbooktype");
		localStorage.removeItem("getbookdate");
		localStorage.removeItem("getbooktime");
		localStorage.removeItem("getbookmode");
		localStorage.removeItem("getbookstatus");
		localStorage.removeItem("getbookapproveby");
	}

	// update function for booking
	const bookUpd = {
		id: bookid,
		userId: bookuserid,
		name: bookname,
		bookDate: bookdate,
		bookTime: booktime,
		roomType: booktype,
		paymentmode: bookmode,
		bookstatus: bookstatus,
		approvedby: bookapproveby
	}
	const updateBooking = (e, bookUpd)=>{
		// preventing the form from reloading
		e.preventDefault()
		props.bookingUpdateMutation({
			variables: bookUpd
		}).then(()=>{
			// notify the user once it is successfully updated
			Toast.fire({
				icon: 'success',
				title: 'Status successfully updated!'
			})

			// call the function to delete data that temporarily saved in localstorage
			deleteCopiedBookData()

			// redirect to cashier page
			// window.location.href = './cashier'
			window.location.reload()
		})
	}



	// get the all the booking data
	const bookings = props.getBookings.bookings
	let pendinglist = <li className="ring">Loading</li>
	let approvedlist = <li className="ring">Loading</li>
	let checkinlist = <li className="ring">Loading</li>
	if (bookings !== undefined) {

		// get all the pending or for verification data
		pendinglist = bookings.map((booking)=>{
			let copyUnApproveData = ()=>{
				localStorage.setItem("getbookid", booking.id);
				localStorage.setItem("getbookuserid", booking.userId);
				localStorage.setItem("getbookname", booking.name);
				localStorage.setItem("getbooktype", booking.roomType);
				localStorage.setItem("getbookdate", booking.bookDate);
				localStorage.setItem("getbooktime", booking.bookTime);
				localStorage.setItem("getbookmode", booking.paymentmode);
				localStorage.setItem("getbookstatus", booking.bookstatus);
				setbookid(localStorage.getItem("getbookid"))
				setbookuserid(localStorage.getItem("getbookuserid"))
				setbookname(localStorage.getItem("getbookname"))
				setbooktype(localStorage.getItem("getbooktype"))
				setbookdate(localStorage.getItem("getbookdate"))
				setbooktime(localStorage.getItem("getbooktime"))
				setbookmode(localStorage.getItem("getbookmode"))
				setbookstatus(localStorage.getItem("getbookstatus"))
			}

			const{id, name, bookTime, bookDate, bookstatus} = booking
			let showbutton = (
	        	<button className="btn" data-toggle="modal" data-target="#modal-booking-update" onClick={()=>{copyUnApproveData(); hideStatusOptions()}}>
	        		<i className="far fa-edit"></i>
	        	</button>
			)
			if (bookstatus !== "Approved" && bookstatus !== "Check-In" && bookstatus !== "Cancel") {
				return(
					<li key={id}>
			            <span>{name}</span>
			            <span>{bookDate}</span>
			            <span>{bookTime}</span>
			            <span>{showbutton}</span>
			        </li>
				)
			}
		})

		// get all the approved bookings
		approvedlist = bookings.map((booking)=>{
			let copyApprovedData = ()=>{
				localStorage.setItem("getbookid", booking.id);
				localStorage.setItem("getbookuserid", booking.userId);
				localStorage.setItem("getbookname", booking.name);
				localStorage.setItem("getbooktype", booking.roomType);
				localStorage.setItem("getbookdate", booking.bookDate);
				localStorage.setItem("getbooktime", booking.bookTime);
				localStorage.setItem("getbookmode", booking.paymentmode);
				localStorage.setItem("getbookstatus", booking.bookstatus);
				setbookid(localStorage.getItem("getbookid"))
				setbookuserid(localStorage.getItem("getbookuserid"))
				setbookname(localStorage.getItem("getbookname"))
				setbooktype(localStorage.getItem("getbooktype"))
				setbookdate(localStorage.getItem("getbookdate"))
				setbooktime(localStorage.getItem("getbooktime"))
				setbookmode(localStorage.getItem("getbookmode"))
				setbookstatus(localStorage.getItem("getbookstatus"))
			}

			const{id,	name, bookDate, bookTime, bookstatus} = booking
			let showbutton = (
	        	<button className="btn" data-toggle="modal" data-target="#modal-booking-update" onClick={()=>{copyApprovedData(); hideStatusOptions()}}>
	        		<i className="far fa-edit"></i>
	        	</button>
			)
			if (bookstatus === "Approved") {
				return(
					<li key={id}>
			            <span>{name}</span>
			            <span>{bookDate}</span>
			            <span>{bookTime}</span>
			            <span>{showbutton}</span>
			        </li>
				)
			}
		})

		// checkin list
		checkinlist = bookings.map((booking)=>{
			const{id, name, roomType, bookDate, bookTime, bookstatus} = booking
			if (bookstatus === "Check-In") {
				return(
					<li key={id}>
			            <span>{name}</span>
			            <span>{roomType}</span>
			            <span>{bookDate}</span>
			            <span className="mobile-hide">{bookTime}</span>
			        </li>
				)
			}
		})	
	}

	// remove the cashier's data in localstorage
	let deleteCopiedCashierData = ()=>{
		localStorage.removeItem("cashfirstname")
		localStorage.removeItem("cashlastname")
		localStorage.removeItem("cashemail")
		localStorage.removeItem("cashpassword")
		localStorage.removeItem("cashcontact")
		localStorage.removeItem("cashaddress")
		localStorage.removeItem("cashrole")
		localStorage.removeItem("cashusername")
		localStorage.removeItem("cashgender")
	}


	// get the specific data of the cashier and temporarily stored in localstorage
	let copyCashierData = ()=>{
		const userlist = props.getUsers.users
		let getUserData
		if(userlist !== undefined){
			getUserData = userlist.map((user)=>{
				if(localStorage.getItem("userId") === user.id){
					localStorage.setItem("cashfirstname", user.firstname)
					localStorage.setItem("cashlastname", user.lastname)
					localStorage.setItem("cashemail", user.email)
					localStorage.setItem("cashpassword", user.password)
					localStorage.setItem("cashcontact", user.contact)
					localStorage.setItem("cashaddress", user.address)
					localStorage.setItem("cashrole", user.role)
					localStorage.setItem("cashusername", user.username)
					localStorage.setItem("cashgender", user.gender)
					setcashfirstname(localStorage.getItem('cashfirstname'))
					setcashlastname(localStorage.getItem('cashlastname'))
					setcashemail(localStorage.getItem('cashemail'))
					setcashpassword(localStorage.getItem('cashpassword'))
					setcashcontact(localStorage.getItem('cashcontact'))
					setcashaddress(localStorage.getItem('cashaddress'))
					setcashrole(localStorage.getItem('cashrole'))
					setcashusername(localStorage.getItem('cashusername'))
					setcashgender(localStorage.getItem('cashgender'))
				}
			})
		}
	}

	/*to check if the password is old or new; if new, it will be encrypted*/
	let finalcashpassword;
	if(cashpassword === localStorage.getItem('cashpassword')){
		finalcashpassword = localStorage.getItem('cashpassword')
	}
	else{
		// encrypt the password
		finalcashpassword = bcrypt.hashSync(cashpassword)
	}

	// update cashier information
	const userUpd = {
		id: cashid,
		firstname: cashfirstname,
		lastname: cashlastname,
		email: cashemail,
		username: cashusername,
		password: finalcashpassword,
		address: cashaddress,
		contact: cashcontact,
		gender: cashgender,
		role: cashrole
	}
	const updateCashier = (e, userUpd)=>{
		e.preventDefault()
		props.userUpdateMutation({
			variables: userUpd
		}).then(()=>{
			Toast.fire({
				icon: 'success',
				title: 'Successfully updated!'
			})

			if(cashfirstname !== localStorage.getItem('firstname')){
				localStorage.setItem('firstname', cashfirstname)
			}
			deleteCopiedCashierData()
			// window.location.href = './cashier'
			window.location.reload()

		})
	}

	

	return(
		<React.Fragment>
			<div className="container-fluid" id="cashier-container">
				<div className="row">
				    <div className="col-lg-12 col-md-12 col-xs-12" id="cashier-row-label">
			    		<div id="cashier-label">	    			
				    		<div className="cashier-img-box">
								<i className="fas fa-user"></i>
							</div>
				    		<div id="cashier-name">
				    			{name}
				    			<button id="cashier-update-icon" data-toggle="modal" data-target="#modal-cashier-update" onClick={()=>{copyCashierData()}}><i className="fas fa-pen-nib"></i></button>
				    		</div>
				    		<div id="cashier-role">{role}</div>
			    		</div>
			    		<div id="booking">
							<h3>Booking Lists</h3>
			    		</div>
				    </div>
				    <div className="col-lg-6 col-md-6 col-xs-12" id="pendingCol">
				    	<ul id="pending-ul">
				    		<li><h5>Pending/For Verification</h5></li>
				    		<li className="list-header">
				    			<span>Name</span>
				    			<span>Date</span>
				    			<span>Time</span>
				    			<span></span>
				    		</li>
				    		{pendinglist}

				    	</ul>
				    </div>
				    <div className="col-lg-6 col-md-6 col-xs-12" id="approvedCol">
				    	<ul id="approved-ul">
				    		<li><h5>Approved</h5></li>
				    		<li className="list-header">
				    			<span>Name</span>
				    			<span>Date</span>
				    			<span>Time</span>
				    			<span></span>
				    		</li>
				    		{approvedlist}
				    	</ul>
				    </div>

				    <div className="col-lg-12 col-md-12 col-xs-12" id="checkinCol">
				    	<ul id="checkin-ul">
				    		<li><h5>Check-In</h5></li>
				    		<li className="list-header">
				    			<span>Name</span>
				    			<span>Room</span>
				    			<span>Date</span>
				    			<span className="mobile-hide">Time</span>
				    		</li>
				    		{checkinlist}
				    	</ul>
				    </div>         
				</div>
				<div className="modal fade" tabIndex="-1" id="modal-cashier-update">
					<div className="modal-dialog modal-lg">
						<div className="modal-content">
							<div className="modal-header mode-modal-header" id="">
								<h6 className="modal-title">Update Cashier Profile</h6>
								<button type="button" className="close" data-dismiss="modal" onClick={()=>deleteCopiedCashierData()}>
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div className="modal-body">
								<form action="" className="form" id="cashier-form-update" onSubmit={(e)=>updateCashier(e,userUpd)}>
									<div className="row">
									    <div className="col-lg-6 col-md-12 col-xs-12 col-form-user">
										    <div className="form-group" hidden>
										        <label htmlFor="">User ID</label>
										        <input type="text" className="form-control" id="updateid" name="updatecashid" value={cashid} onChange={(e)=> setcashid(e.target.value)} required/>
										    </div>
										    <div className="form-group">
										        <label htmlFor="">First Name</label>
										        <input type="text" className="form-control" id="updatefirstname" name="updatefirstname" value={cashfirstname} onChange={(e)=> setcashfirstname(e.target.value)} required/>
										    </div>
										    <div className="form-group">
										        <label htmlFor="">Last Name</label>
										        <input type="text" className="form-control" id="updatelastname" name="updatelastname" value={cashlastname} onChange={(e)=> setcashlastname(e.target.value)} required/>
										    </div>
										    <div className="form-group">
										        <label htmlFor="">Email</label>
										        <input type="email" className="form-control" id="updateemail" name="updateemail" value={cashemail} onChange={(e)=> setcashemail(e.target.value)} required/>
										    </div>
										    <div className="form-group" hidden>
										        <label htmlFor="">Username</label>
										        <input type="text" className="form-control" id="updateusername" name="updateusername" value={cashusername} onChange={(e)=> setcashusername(e.target.value)} required/>
										    </div>
										    <div className="form-group">
										        <label htmlFor="">Password</label>
										        <input type="password" className="form-control" id="updatepassword" name="updatepassword" value={cashpassword} onChange={(e)=> setcashpassword(e.target.value)} required/>
										    </div>
									    </div>        
									    <div className="col-lg-6 col-md-12 col-xs-12 col-form-user">
										    <div className="form-group" hidden>
										        <label htmlFor="">Confirm Password</label>
										        <input type="password" className="form-control" id="updateconfirm" name="updateconfirm"/>
										    </div>
										    <div className="form-group">
										        <label htmlFor="">Address</label>
										        <input type="text" className="form-control" name="updateaddress" id="updateaddress" value={cashaddress} onChange={(e)=> setcashaddress(e.target.value)} required/>

										    </div>
										    <div className="form-group">
										        <label htmlFor="">Contact</label>
										        <input type="text" className="form-control" id="updatecontact" name="updatecontact" value={cashcontact} onChange={(e)=> setcashcontact(e.target.value)} required/>
										    </div>
										    <div className="form-group">
										        <label htmlFor="gender">Gender</label>
										        <select className="form-control" name="updategender" id="updategender" value={cashgender} onChange={(e)=> setcashgender(e.target.value)} required>
										            <option value="Male">Male</option>
										            <option value="Female">Female</option>
										        </select>
										    </div>
										    <div className="form-group" hidden>
										        <label htmlFor="role">Role</label>
										         <input type="text" className="form-control" name="updaterole" id="updaterole" value={cashrole} onChange={(e)=> setcashrole(e.target.value)} readOnly/>
										    </div>
										    <button type="submit" className="btn" id="customer-modal-update">Update</button>
									    </div>        
									</div>
								</form>
							</div>	
						</div>
					</div>
				</div>

				<div className="modal fade" tabIndex="-1" id="modal-booking-update">
					<div className="modal-dialog modal-lg">
						<div className="modal-content">
							<div className="modal-header mode-modal-header" id="">
								<h6 className="modal-title">Booking Data</h6>
								<button type="button" className="close" data-dismiss="modal" onClick={()=>deleteCopiedBookData()}>
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div className="modal-body">
								<form action="" className="form" onSubmit={(e)=>updateBooking(e,bookUpd)}>
								    <div className="row">
									    <div className="col-lg-6 col-md-12 col-xs-12">
										    <div className="form-group" hidden>
										        <label htmlFor="">Booking Id</label>
										        <input type="text" className="form-control" id="bookId" name="bookId" value={bookid} onChange={(e)=> setbookid(e.target.value)} disabled/>
										    </div>
										    <div className="form-group" hidden>
										        <label htmlFor="">User Id</label>
										        <input type="text" className="form-control" id="userId" name="userId" value={bookuserid} onChange={(e)=> setbookuserid(e.target.value)} disabled/>
										    </div>
										    <div className="form-group">
										        <label htmlFor="name">Name</label>
										        <input type="text" className="form-control" id="name" name="name" value={bookname} onChange={(e)=> setbookname(e.target.value)} disabled required/>
										    </div>
										    <div className="form-group">
										        <label htmlFor="roomType">Room</label>
										        <input type="text" className="form-control" id="roomType" name="roomType" value={booktype} onChange={(e)=> setbooktype(e.target.value)} required disabled/>
										    </div>
										    <div className="form-group">
										        <label htmlFor="bookDate">Book Date</label>
										        <input type="date" className="form-control" id="bookDate" name="bookDate" value={bookdate} onChange={(e)=> setbookdate(e.target.value)} disabled required/>
										    </div>
									    </div>
									    <div className="col-lg-6 col-md-12 col-xs-12">
										    <div className="form-group">
										        <label htmlFor="bookTime">Book Time</label>
										        <input type="time" className="form-control" id="bookTime" name="bookTime" value={booktime} onChange={(e)=> setbooktime(e.target.value)} disabled required/>
										    </div>
										    <div className="form-group">
										        <label htmlFor="paymentmode">Payment Mode</label>
										        <input type="text" className="form-control" id="paymentmode" name="paymentmode" value={bookmode} onChange={(e)=> setbookmode(e.target.value)} disabled required/>
										    </div>
										    <div className="form-group">
										        <label htmlFor="bookstatus">Book Status</label>									     
										        <select className="form-control" id="bookstatus" name="bookstatus" value={bookstatus} onChange={(e)=> setbookstatus(e.target.value)}  required>
										            <option value="Pending" id="optionpending">Pending</option>
										            <option value="Approved">Approved</option>
										            <option value="Check-In" id="optioncheckin">Check-In</option>
										            <option value="Cancel">Cancel</option>
										        </select>

										    </div>
										    <button type="submit" className="btn btn-primary" id="booking-modal-submit">Submit</button>
									    </div>
								    </div>
								</form>
							</div>	
						</div>
					</div>
				</div>
			</div>
			<Footer/>
		</React.Fragment>
	)
}
export default compose(
	graphql(bookingUpdateMutation, {name: 'bookingUpdateMutation'}),
	graphql(getBookings, {name: 'getBookings'}),

	graphql(getUsers, {name: 'getUsers'}),
	graphql(userUpdateMutation, {name: 'userUpdateMutation'}),
)(CashierPage)
